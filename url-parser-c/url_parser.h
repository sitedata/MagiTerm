#ifndef __URL_PARSER_H__
#define __URL_PARSER_H__

typedef struct url_parser_url {
  char *protocol;
  char *host;
  int port;
  char *path;
  char *query_string;
} url_parser_url_t;

extern void free_parsed_url(url_parser_url_t *url_parsed);
extern int parse_url(char *url, bool verify_host, url_parser_url_t *parsed_url);
#endif