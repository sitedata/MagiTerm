#ifndef __YESNO_H__
#define __YESNO_H__

#include <string>

class Terminal;

class yesno {
public:
  yesno(Terminal *term, std::string p);
  bool enter() { return highlighted; }
  void draw();
  bool process(char *data, bool *out);
  void tab();

private:
  Terminal *t;
  std::string prompt;
  bool highlighted;
};

#endif
