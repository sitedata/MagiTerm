#ifndef __FILECHOOSER_H__
#define __FILECHOOSER_H__
#include <limits.h>
#include "Terminal.h"

#ifdef _MSC_VER
#define PATH_MAX MAX_PATH
#endif

class filechooser {
public:
  filechooser(Terminal *term);
  void load();
  void draw();
  void up();
  void down();
  void pgup();
  void pgdown();
  void tab();
  char *enter();
  ~filechooser();

private:
  Terminal *t;
  char current_path[PATH_MAX];
  int load_dir();
  void draw_bg();
  void draw_dirs();
  void draw_files();
  char **dirs;
  char **files;
  int dir_count;
  int file_count;
  int dir_top;
  int file_top;
  int file_at;
  int dir_at;
  int focus;
};

#endif
