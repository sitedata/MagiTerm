#include <limits.h>
#include <algorithm>
#include <inttypes.h>
#include "dialdirectory.h"
#include "yesno.h"
#include "mtermans.h"
#include "inih/ini.h"

#ifdef _MSC_VER
#include <wincred.h>
#include <Shlobj.h>
#include <shellapi.h>
#define strcasecmp stricmp
#define PATH_MAX MAX_PATH
#else
#include <pwd.h>
#ifdef __APPLE__
#include <CoreFoundation/CoreFoundation.h>
#include <Security/Security.h>

template <typename T> struct Releaser {
  explicit Releaser(const T &v) : value(v) {}
  ~Releaser() { CFRelease(value); }
  const T value;
};

#else
#ifdef __HAIKU__
#include <Key.h>
#include <KeyStore.h>
const char *kMagitermKeyringName = "MagiTerm";
#else
#include <libsecret/secret.h>

const SecretSchema *magiterm_get_schema(void) {
  static const SecretSchema the_schema = {"com.magickabbs.magiterm.Password",
                                          SECRET_SCHEMA_NONE,
                                          {
                                              {"bbsname", SECRET_SCHEMA_ATTRIBUTE_STRING},
                                              {"username", SECRET_SCHEMA_ATTRIBUTE_STRING},
                                              {NULL, (SecretSchemaAttributeType)0},
                                          }};
  return &the_schema;
}

#define MAGITERM_SCHEMA magiterm_get_schema()
#endif
#endif
#endif
#ifdef _MSC_VER
extern std::string utf16ToUTF8(const std::wstring &s);
#endif

dialdir::dialdir(Terminal *term, int width, int height) {
  t = term;
  cur_item = 0;
  state = 0;
  top = 0;
  memset(searchdata, 0, 257);
  term_width = width;
  term_height = height;
  new_width = 0;
  new_height = 0;
#ifdef _MSC_VER
  WCHAR *homedir;

  SHGetKnownFolderPath(FOLDERID_Downloads, 0, NULL, &homedir);
  default_download_directory = utf16ToUTF8(homedir);
#else
  struct passwd *pw = getpwuid(getuid());

  const char *homedir = pw->pw_dir;

  default_download_directory = homedir;
#endif
}

void dialdir::resize(int width, int height) {
  new_width = width;
  new_height = height;
}

static int handler(void *user, const char *section, const char *name, const char *value) {
  int i;

  std::vector<struct dialinfo> *entries = (std::vector<struct dialinfo> *)user;

  for (i = 0; i < entries->size(); i++) {
    if (strcmp(section, entries->at(i).name.c_str()) == 0) {
      if (strcasecmp(name, "address") == 0) {
        entries->at(i).address = value;
      } else if (strcasecmp(name, "port") == 0) {
        entries->at(i).port = atoi(value);
      } else if (strcasecmp(name, "username") == 0) {
        entries->at(i).username = value;
      } else if (strcasecmp(name, "conn type") == 0) {
        entries->at(i).type = atoi(value);
      } else if (strcasecmp(name, "pinned") == 0) {
        entries->at(i).pinned = (strcasecmp(value, "true") == 0);
      } else if (strcasecmp(name, "memo") == 0) {
        entries->at(i).memo = value;
      } else if (strcasecmp(name, "download-dir") == 0) {
        entries->at(i).downloads = value;
      }
      return 1;
    }
  }

  struct dialinfo entry;

  entry.name = section;
  entry.username = "";
  entry.address = "";
  entry.port = 22;
  entry.type = 0;
  entry.pinned = false;
  entry.memo = "";
  entry.downloads = "";
  if (strcasecmp(name, "address") == 0) {
    entry.address = value;
  } else if (strcasecmp(name, "port") == 0) {
    entry.port = atoi(value);
  } else if (strcasecmp(name, "username") == 0) {
    entry.username = value;
  } else if (strcasecmp(name, "memo") == 0) {
    entry.memo = value;
  } else if (strcasecmp(name, "download-dir") == 0) {
    entry.downloads = value;
  } else if (strcasecmp(name, "conn type") == 0) {
    entry.type = atoi(value);
  } else if (strcasecmp(name, "pinned") == 0) {
    entry.pinned = (strcasecmp(value, "true") == 0);
  }

  entries->push_back(entry);
  return 1;
}

static int sync_handler(void *user, const char *section, const char *name, const char *value) {
  int i;

  std::vector<struct dialinfo> *entries = (std::vector<struct dialinfo> *)user;

  if (strlen(section) == 0)
    return 1;

  const char *trimmed_name = name;
  for (int i = 0; i < strlen(name); i++) {
    if (name[i] != ' ' && name[i] != '\t') {
      trimmed_name = &name[i];
      break;
    }
  }

  for (i = 0; i < entries->size(); i++) {
    if (strcmp(section, entries->at(i).name.c_str()) == 0) {
      if (strcasecmp(trimmed_name, "address") == 0) {
        entries->at(i).address = value;
      } else if (strcasecmp(trimmed_name, "port") == 0) {
        entries->at(i).port = atoi(value);
      } else if (strcasecmp(trimmed_name, "connectiontype") == 0) {
        if (strcasecmp(value, "Telnet") == 0) {
          if (entries->at(i).port == -1) {
            entries->at(i).port = 23;
          }
          entries->at(i).type = 1;
        } else if (strcasecmp(value, "SSH") == 0) {
          if (entries->at(i).port == -1) {
            entries->at(i).port = 22;
          }
          entries->at(i).type = 0;
        } else {
          entries->at(i).type = -1;
        }
      }

      return 1;
    }
  }

  struct dialinfo entry;

  entry.name = section;
  entry.username = "";
  entry.address = "";
  entry.port = -1;
  entry.type = 1;
  entry.pinned = false;
  entry.memo = "Imported from Syncterm";
  entry.downloads = "";

  if (strcasecmp(trimmed_name, "address") == 0) {
    entry.address = value;
  } else if (strcasecmp(trimmed_name, "port") == 0) {
    entry.port = atoi(value);
  } else if (strcasecmp(trimmed_name, "connectiontype") == 0) {
    if (strcasecmp(value, "Telnet") == 0) {
      if (entry.port == -1) {
        entry.port = 23;
      }
      entry.type = 1;
    } else if (strcasecmp(value, "SSH") == 0) {
      if (entry.port == -1) {
        entry.port = 22;
      }
      entry.type = 0;
    } else {
      entry.type = -1;
    }
  }
  entries->push_back(entry);
  return 1;
}

void dialdir::savepass(int entry, const char *username, const char *password) {
  if (entry == -1) {
    entry = cur_item;
  }
  if (entries.at(entry).username == "") {
    entries.at(entry).username = username;
    save();
  }
  storepassword(entry, username, password);
}

void dialdir::storepassword(int entry, const char *username, const char *password) {
#ifdef _MSC_VER
  DWORD cbCreds = 1 + strlen(password);
  char target[512];
  CREDENTIAL cred = {0};

  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());

  cred.Type = CRED_TYPE_GENERIC;
  cred.TargetName = (LPSTR)target;
  cred.CredentialBlobSize = cbCreds;
  cred.CredentialBlob = (LPBYTE)password;
  cred.Persist = CRED_PERSIST_LOCAL_MACHINE;
  cred.UserName = (LPSTR)username;
  ::CredWrite(&cred, 0);
#else
#ifdef __APPLE__
  char target[512];
  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());

  const OSStatus ret = SecKeychainAddGenericPassword(NULL, strlen(target), target, strlen(username), username, strlen(password), password, NULL);

  Releaser<CFStringRef> str(SecCopyErrorMessageString(ret, 0));
  printf("%s\n", CFStringGetCStringPtr(str.value, kCFStringEncodingUTF8));

#else
#ifdef __HAIKU__
  BKeyStore keyStore;
  BPasswordKey key;

  status_t status = keyStore.GetKey(kMagitermKeyringName, B_KEY_TYPE_PASSWORD, entries.at(entry).name.c_str(), username, key);
  // Fill out the key with existing data, or create new data
  switch (status) {
  case B_OK:
    // Remove the existing key
    keyStore.RemoveKey(kMagitermKeyringName, key);
    // Update the password
    key.SetPassword(password);
    break;
  case B_BAD_VALUE:
    keyStore.AddKeyring(kMagitermKeyringName);
  default:
    key.SetTo(password, B_KEY_PURPOSE_GENERIC, entries.at(entry).name.c_str(), username);
    break;
  }

  // Store the updated/new key in the keyring
  keyStore.AddKey(kMagitermKeyringName, key);

  // Nothing yet
#else
  char secret_title[512];

  snprintf(secret_title, 512, "MagiTerm/%s", entries.at(entry).name.c_str());

  secret_password_store_sync(MAGITERM_SCHEMA, SECRET_COLLECTION_DEFAULT, secret_title, password, NULL, NULL, "bbsname", entries.at(entry).name.c_str(),
                             "username", username, nullptr);
#endif
#endif
#endif
}

char *dialdir::getpass(int item) {
  char *pass;

  if (item == -1) {
    item = cur_item;
  }

  if (entries.at(item).username == "") {
    return NULL;
  }

  if (retrievepassword(item, entries.at(item).username.c_str(), &pass) == 0) {
    return pass;
  } else {
    return NULL;
  }
}

int dialdir::retrievepassword(int entry, const char *username, char **password) {
#ifdef _MSC_VER
  PCREDENTIAL pcred;
  char target[512];
  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());
  BOOL ok = ::CredRead((LPCSTR)target, CRED_TYPE_GENERIC, 0, &pcred);
  if (!ok) {
    return 1;
  }
  *password = (char *)malloc(pcred->CredentialBlobSize + 1);
  memset(*password, 0, pcred->CredentialBlobSize + 1);
  memcpy(*password, (char *)pcred->CredentialBlob, pcred->CredentialBlobSize);
  ::CredFree(pcred);
  return 0;
#else
#ifdef __APPLE__
  char target[512];
  Uint32 len;
  char *data;
  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());
  const OSStatus ret = SecKeychainFindGenericPassword(NULL, // default keychain
                                                      strlen(target), target, strlen(username), username, &len, (void **)&data, 0);

  if (ret == noErr) {
    *password = (char *)malloc(len + 1);
    memset(*password, 0, len + 1);
    memcpy(*password, data, len);
    SecKeychainItemFreeContent(NULL, data);
    return 0;
  } else {
    return 1;
  }
#else
#ifdef __HAIKU__
  // There may be more than one match, so we use the iteration methods.
  BKeyStore keyStore;
  BPasswordKey key;

  status_t status = keyStore.GetKey(kMagitermKeyringName, B_KEY_TYPE_PASSWORD, entries.at(entry).name.c_str(), username, key);

  switch (status) {
  case B_OK:
    *password = (char *)malloc(strlen(key.Password()) + 1);
    if (!*password) {
      return 1;
    }
    memset(*password, 0, strlen(key.Password()) + 1);
    strncpy(*password, key.Password(), strlen(key.Password()));
    return 0;
    break;
  case B_BAD_VALUE:
    keyStore.AddKeyring(kMagitermKeyringName);
    break;
  default:
    break;
  }
  return 1;
#else
  GError *error = NULL;

  /* The attributes used to lookup the password should conform to the schema. */
  gchar *spassword = secret_password_lookup_sync(MAGITERM_SCHEMA, NULL, &error, "bbsname", entries.at(entry).name.c_str(), "username", username, nullptr);

  if (error != NULL) {
    /* ... handle the failure here */
    g_error_free(error);

  } else if (spassword == NULL) {
    /* password will be null, if no matching password found */
    return 1;
  } else {
    /* ... do something with the password */
    *password = strdup(spassword);
    secret_password_free(spassword);
    return 0;
  }
#endif
#endif
#endif
  return 1;
}

void dialdir::wipepassword(int entry, const char *username) {
#ifdef _MSC_VER
  char target[512];
  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());
  ::CredDelete((LPCSTR)target, CRED_TYPE_GENERIC, 0);
#else
#ifdef __APPLE__
  char target[512];
  int len;
  char *data;
  SecKeychainItemRef ref;
  snprintf(target, 512, "MagiTerm/%s", entries.at(entry).name.c_str());
  SecKeychainFindGenericPassword(NULL, // default keychain
                                 strlen(target), target, strlen(username), username, NULL, NULL, &ref);
  const Releaser<SecKeychainItemRef> releaser(ref);

  SecKeychainItemDelete(ref);

#else
#ifdef __HAIKU__
  // nothing yet
  BKeyStore keyStore;
  BPasswordKey key;

  status_t status = keyStore.GetKey(kMagitermKeyringName, B_KEY_TYPE_PASSWORD, entries.at(entry).name.c_str(), username, key);

  switch (status) {
  case B_OK:
    keyStore.RemoveKey(kMagitermKeyringName, key);
    break;
  default:
    break;
  }
#else
  GError *error = NULL;
  secret_password_clear_sync(MAGITERM_SCHEMA, NULL, &error, "bbsname", entries.at(entry).name.c_str(), "username", username, nullptr);
  if (error != NULL) {
    g_error_free(error);
  }
#endif
#endif
#endif
}

void dialdir::doimport(char *importpath) {
  if (strlen(importpath) < 4)
    return;
  if (strcasecmp(&importpath[strlen(importpath) - 4], ".ini") == 0) {
    ini_parse(importpath, handler, &entries);
  } else if (strcasecmp(&importpath[strlen(importpath) - 4], ".lst") == 0) {
    ini_parse(importpath, sync_handler, &entries);
    for (int i = entries.size() - 1; i >= 0; i--) {
      if (entries.at(i).type == -1) {
        entries.erase(entries.begin() + i);
      }
    }
  }
  sort();
  save();
}

void dialdir::load() {
  char buffer[PATH_MAX];
  char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
  snprintf(buffer, PATH_MAX, "%sdialdirectory.ini", prefpath);

  ini_parse(buffer, handler, &entries);
  SDL_free(prefpath);
  sort();
}

void dialdir::doexport(char *exportpath) {
  FILE *fptr;
  int i;

  fptr = fopen(exportpath, "w");

  if (fptr) {
    for (i = 0; i < entries.size(); i++) {
      fprintf(fptr, "[%s]\n", entries.at(i).name.c_str());
      fprintf(fptr, "address = %s\n", entries.at(i).address.c_str());
      fprintf(fptr, "port = %d\n", entries.at(i).port);
      fprintf(fptr, "conn type = %d\n", entries.at(i).type);
      fprintf(fptr, "pinned = %s\n", entries.at(i).pinned ? "true" : "false");
      if (entries.at(i).memo != "") {
        fprintf(fptr, "memo = %s\n", entries.at(i).memo.c_str());
      }
      if (entries.at(i).downloads != "") {
        fprintf(fptr, "download-dir = %s\n", entries.at(i).downloads.c_str());
      }
      fprintf(fptr, "\n");
    }
    fclose(fptr);
  }
}

static bool ddirCompare(struct dialinfo a, struct dialinfo b) {
  if (a.pinned && b.pinned) {
    return (strcmp(a.name.c_str(), b.name.c_str()) < 0);
  }

  if (!a.pinned && !b.pinned) {
    return (strcmp(a.name.c_str(), b.name.c_str()) < 0);
  }

  if (a.pinned && !b.pinned) {
    return true;
  }
  if (!a.pinned && b.pinned) {
    return false;
  }

  return true;
}

void dialdir::sort() { std::sort(entries.begin(), entries.end(), ddirCompare); }

void dialdir::pin_entry() {
  std::string entryname = entries.at(cur_item).name;
  entries.at(cur_item).pinned = !entries.at(cur_item).pinned;
  sort();
  save();

  cur_item = 0;

  for (int i = 0; i < entries.size(); i++) {
    if (entries.at(i).name == entryname) {
      cur_item = i;
      break;
    }
  }

  display();
}

void dialdir::save() {
  char buffer[PATH_MAX];
  char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
  FILE *fptr;
  int i;

  snprintf(buffer, PATH_MAX, "%sdialdirectory.ini", prefpath);

  fptr = fopen(buffer, "w");

  if (fptr) {
    for (i = 0; i < entries.size(); i++) {
      fprintf(fptr, "[%s]\n", entries.at(i).name.c_str());
      fprintf(fptr, "address = %s\n", entries.at(i).address.c_str());
      fprintf(fptr, "port = %d\n", entries.at(i).port);
      fprintf(fptr, "conn type = %d\n", entries.at(i).type);
      if (entries.at(i).memo != "") {
        fprintf(fptr, "memo = %s\n", entries.at(i).memo.c_str());
      }
      if (entries.at(i).pinned) {
        fprintf(fptr, "pinned = true\n");
      }
      if (entries.at(i).downloads != "") {
        fprintf(fptr, "download-dir = %s\n", entries.at(i).downloads.c_str());
      }
      if (entries.at(i).username != "") {
        fprintf(fptr, "username = %s\n", entries.at(i).username.c_str());
      }

      fprintf(fptr, "\n");
    }
    fclose(fptr);
  }
  SDL_free(prefpath);
}

void dialdir::display() {
  int i;
  char buffer[256];
  char *ptr;
  if (state == 0) {
    if (new_width != 0 && new_height != 0) {
      term_width = new_width;
      term_height = new_height;
      new_width = 0;
      new_height = 0;
    }
    t->twrite((unsigned char *)"\x1b[0m\x1b[2J\x1b[1;1H", 14);
    t->twrite((unsigned char *)MAGITERM_ANSI_DATA, sizeof(MAGITERM_ANSI_DATA));

    if (!display_memo) {
      snprintf(buffer, 256, "\x1b[13;0H\x1b[0;46;30mName                                    Address\x1b[K");
    } else {
      snprintf(buffer, 256, "\x1b[13;0H\x1b[0;46;30mName                                    Memo\x1b[K");
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));

    snprintf(buffer, 256, "\x1b[%d;0H\x1b[0;46;30mPress Alt-Z to toggle help\x1b[K", term_height);
    t->twrite((unsigned char *)buffer, strlen(buffer));

    if (cur_item > top + (term_height - 15)) {
      top = cur_item - (term_height - 15);
    } else if (cur_item < top) {
      top = cur_item;
    }

    if (entries.size() == 0) {
      t->twrite((unsigned char *)"\x1b[16;35HNo Entries Found", 24);
    } else {
      t->twrite((unsigned char *)"\x1b[14;1H", 7);
      for (i = top; i < entries.size() && i < top + (term_height - 14); i++) {
        ptr = getpass(i);
        if (i == cur_item) {
          if (!display_memo) {
            snprintf(buffer, 256, "\x1b[%d;1H\x1b[36;47m%c%s\x1b[1;33m%c \x1b[0;47;30m%-35.35s %32.32s %d\x1b[K", i - top + 14,
                     (entries.at(i).pinned ? '\xaf' : ' '), (entries.at(i).type == 0 ? "\x1b[1;32mS" : "\x1b[1;31mT"), (ptr != NULL ? '*' : ' '),
                     entries.at(i).name.c_str(), entries.at(i).address.c_str(), entries.at(i).port);
          } else {
            snprintf(buffer, 256, "\x1b[%d;1H\x1b[36;47m%c%s\x1b[1;33m%c \x1b[0;47;30m%-35.35s %-35.35s\x1b[K", i - top + 14,
                     (entries.at(i).pinned ? '\xaf' : ' '), (entries.at(i).type == 0 ? "\x1b[1;32mS" : "\x1b[1;31mT"), (ptr != NULL ? '*' : ' '),
                     entries.at(i).name.c_str(), entries.at(i).memo.c_str());
          }
        } else {
          if (!display_memo) {
            snprintf(buffer, 256, "\x1b[%d;1H\x1b[36;40m%c%s\x1b[1;33m%c \x1b[0m%-35.35s %32.32s %d\x1b[K", i - top + 14, (entries.at(i).pinned ? '\xaf' : ' '),
                     (entries.at(i).type == 0 ? "\x1b[1;32mS" : "\x1b[1;31mT"), (ptr != NULL ? '*' : ' '), entries.at(i).name.c_str(),
                     entries.at(i).address.c_str(), entries.at(i).port);
          } else {
            snprintf(buffer, 256, "\x1b[%d;1H\x1b[36;40m%c%s\x1b[1;33m%c \x1b[0m%-35.35s %-35.35s\x1b[K", i - top + 14, (entries.at(i).pinned ? '\xaf' : ' '),
                     (entries.at(i).type == 0 ? "\x1b[1;32mS" : "\x1b[1;31mT"), (ptr != NULL ? '*' : ' '), entries.at(i).name.c_str(),
                     entries.at(i).memo.c_str());
          }
        }
        free(ptr);
        t->twrite((unsigned char *)buffer, strlen(buffer));
      }
    }
  }
}

int dialdir::enter() {
  char buffer[256];
  int i;
  if (state == 0) {
    if (entries.size() > 0) {
      return 1;
    } else {
      return 0;
    }
  } else if (state == 1) {
    state = 2;
    updateCursor();
    /*
            if (strlen(tmpbbs) > 0) {
                for (i=0;i<entry_count;i++) {
                    if (strcmp(tmpbbs, entries[i]->name) == 0) {
                        return 0;
                    }
                }
                snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;24H");
                t->twrite((unsigned char *)buffer, strlen(buffer));
                state = 2;
            }
    */
  } else if (state == 2) {
    state = 3;
    updateCursor();
    //        if (strlen(tmphost) > 0) {
    //            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H%d", tmpport);
    //            t->twrite((unsigned char *)buffer, strlen(buffer));
    //            state = 3;
    //        }
  } else if (state == 3) {
    state = 4;
    updateCursor();
  } else if (state == 4) {
    state = 5;
    updateCursor();
  } else if (state == 5) {
    state = 6;
    updateCursor();
  } else if (state == 6) {
    state = 7;
    updateCursor();
  } else if (state == 20) {
    if (del_prompt->enter() == true) {
      do_remove();
      save();
    }
    delete del_prompt;
    state = 0;
    display();
  } else if (state == 7) {
    state = 8;
    updateCursor();
  } else if (state == 8) {
    state = 9;
    updateCursor();
  } else if (state == 9) {
    state = 10;
    updateCursor();
  } else if (state == 10) {
    snprintf(buffer, 256, "\x1b[0;46;30m\x1b[12;32H                     \x1b[0;46;30m\x1b[14;32H                     ");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    if (strlen(tmpbbs) == 0) {
      snprintf(buffer, 256, "\x1b[0;46;31m\x1b[8;32HCan not be empty!");
      t->twrite((unsigned char *)buffer, strlen(buffer));
      state = 1;
      updateCursor();
      return 0;
    }
    if (strlen(tmphost) == 0) {
      snprintf(buffer, 256, "\x1b[0;46;31m\x1b[10;32HCan not be empty!");
      t->twrite((unsigned char *)buffer, strlen(buffer));
      state = 2;
      updateCursor();
      return 0;
    }
    if (tmpport == 0) {
      if (tmptype == 0) {
        tmpport = 22;
      } else if (tmptype == 1) {
        tmpport = 23;
      }
    }
    if (editing == -1) {
      for (i = 0; i < entries.size(); i++) {
        if (strcasecmp(tmpbbs, entries.at(i).name.c_str()) == 0) {
          snprintf(buffer, 256, "\x1b[0;46;31m\x1b[8;32HDuplicate Entry!");
          t->twrite((unsigned char *)buffer, strlen(buffer));
          state = 1;
          updateCursor();
          return 0;
        }
      }

      struct dialinfo entry;

      entry.name = tmpbbs;
      entry.address = tmphost;
      entry.port = tmpport;
      entry.pinned = false;
      entry.memo = tmpmemo;
      entry.username = tmpuser;
      entry.type = tmptype;
      entry.downloads = tmpdownload;
      entries.push_back(entry);
      if (entry.username != "" && strlen(tmppass) > 0) {
        savepass(entries.size() - 1, entry.username.c_str(), tmppass);
      }
    } else {
      if (entries.at(editing).username != "") {
        wipepassword(editing, entries.at(editing).username.c_str());
        entries.at(editing).username = "";
      }

      entries.at(editing).name = tmpbbs;
      entries.at(editing).address = tmphost;
      entries.at(editing).port = tmpport;
      entries.at(editing).type = tmptype;
      entries.at(editing).memo = tmpmemo;
      entries.at(editing).downloads = tmpdownload;
      if (strlen(tmpuser) > 0) {
        entries.at(editing).username = tmpuser;
        if (strlen(tmppass) > 0) {
          savepass(editing, entries.at(editing).username.c_str(), tmppass);
        }
      }
    }
    sort();
    save();

    state = 0;
    display();
  }
  return 0;
}

void dialdir::backspace() {
  char buffer[256];
  char *ptr;
  int i;
  if (state == 0) {
    if (strlen(searchdata) > 0) {
      searchdata[strlen(searchdata) - 1] = '\0';
      t->twrite("\x1b[0;46;30m\x1b[" + std::to_string(term_height) + ";30HSearch: " + std::string(searchdata) + "\x1b[K");
    }
  } else if (state == 1) {
    if (strlen(tmpbbs) > 0) {
      tmpbbs[strlen(tmpbbs) - 1] = '\0';
      if (strlen(tmpbbs) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H<%s\x1b[s \x1b[u", &tmpbbs[strlen(tmpbbs) - 31]);
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H %s\x1b[s \x1b[u", tmpbbs);
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 2) {
    if (strlen(tmphost) > 0) {
      tmphost[strlen(tmphost) - 1] = '\0';
      if (strlen(tmphost) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H<%s\x1b[s \x1b[u", &tmphost[strlen(tmphost) - 31]);
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H %s\x1b[s \x1b[u", tmphost);
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 3) {
    tmpport = tmpport / 10;
    if (tmpport == 0) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;24H\x1b[s \x1b[u");
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;24H%d\x1b[s \x1b[u", tmpport);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 6) {
    if (strlen(tmpuser) > 0) {
      tmpuser[strlen(tmpuser) - 1] = '\0';
      if (strlen(tmpuser) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H<%s\x1b[s \x1b[u", &tmpuser[strlen(tmpuser) - 31]);
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H %s\x1b[s \x1b[u", tmpuser);
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 7) {
    if (strlen(tmppass) > 0) {
      tmppass[strlen(tmppass) - 1] = '\0';
      if (strlen(tmppass) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H<");
        ptr = &buffer[strlen(buffer)];
        for (i = 31; i < strlen(tmppass); i++) {
          *ptr++ = '*';
          *ptr = '\0';
        }
        strcat(buffer, "\x1b[s \x1b[u");
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H ");
        ptr = &buffer[strlen(buffer)];
        for (i = 0; i < strlen(tmppass); i++) {
          *ptr++ = '*';
          *ptr = '\0';
        }
        strcat(buffer, "\x1b[s \x1b[u");
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 8) {
    if (strlen(tmpmemo) > 0) {
      tmpmemo[strlen(tmpmemo) - 1] = '\0';
      if (strlen(tmpmemo) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s\x1b[s \x1b[u", &tmpmemo[strlen(tmpmemo) - 31]);
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s\x1b[s \x1b[u", tmpmemo);
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 9) {
    if (strlen(tmpdownload) > 0) {
      tmpdownload[strlen(tmpdownload) - 1] = '\0';
      if (strlen(tmpdownload) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<%s\x1b[s \x1b[u", &tmpdownload[strlen(tmpdownload) - 31]);
      } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H %s\x1b[s \x1b[u", tmpdownload);
      }
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  }
}

void dialdir::process(char *data) {
  char buffer[257];
  char *ptr;
  int i;
  if (state == 0) {
    strncat(searchdata, data, 256);
    for (i = 0; i < entries.size(); i++) {
#ifdef _MSC_VER
      if (_strnicmp(searchdata, entries.at(i).name.c_str(), (strlen(searchdata) > 256 ? 256 : strlen(searchdata))) == 0) {
#else
      if (strncasecmp(searchdata, entries.at(i).name.c_str(), (strlen(searchdata) > 256 ? 256 : strlen(searchdata))) == 0) {
#endif
        cur_item = i;
        display();
        break;
      }
    }
    t->twrite("\x1b[0;46;30m\x1b[" + std::to_string(term_height) + ";30HSearch: " + std::string(searchdata));
  } else if (state == 1) {
    if (strlen(tmpbbs) + strlen(data) > 200) {
      return;
    }
    strcat(tmpbbs, data);
    if (strlen(tmpbbs) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H<%s", &tmpbbs[strlen(tmpbbs) - 31]);
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H %s", tmpbbs);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 2) {
    if (strlen(tmphost) + strlen(data) > 200) {
      return;
    }
    strcat(tmphost, data);
    if (strlen(tmphost) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H<%s", &tmphost[strlen(tmphost) - 31]);
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H %s", tmphost);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 3) {
    for (i = 0; i < strlen(data); i++) {
      tmpport = tmpport * 10 + (data[i] - '0');
    }
    if (tmpport == 0) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H ");
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H %d", tmpport);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 4) {
    if (data[0] == ' ') {
      tmptype = 0;
      snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |\x1b[14;24H", (tmptype == 0 ? 'X' : ' '),
               (tmptype == 1 ? 'X' : ' '));
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }

  } else if (state == 5) {
    if (data[0] == ' ') {
      tmptype = 1;
      snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |\x1b[14;33H", (tmptype == 0 ? 'X' : ' '),
               (tmptype == 1 ? 'X' : ' '));
      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  } else if (state == 6) {
    if (strlen(tmpuser) + strlen(data) > 200) {
      return;
    }
    strcat(tmpuser, data);
    if (strlen(tmpuser) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H<%s", &tmpuser[strlen(tmpuser) - 31]);
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H %s", tmpuser);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  }

  else if (state == 7) {
    if (strlen(tmppass) + strlen(data) > 200) {
      return;
    }
    strcat(tmppass, data);
    if (strlen(tmppass) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H<");
      ptr = &buffer[strlen(buffer)];
      for (i = 31; i < strlen(tmppass); i++) {
        *ptr++ = '*';
        *ptr = '\0';
      }
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H ");
      ptr = &buffer[strlen(buffer)];
      for (i = 0; i < strlen(tmppass); i++) {
        *ptr++ = '*';
        *ptr = '\0';
      }
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 8) {
    if (strlen(tmpmemo) + strlen(data) > 200) {
      return;
    }
    strcat(tmpmemo, data);
    if (strlen(tmpmemo) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s", &tmpmemo[strlen(tmpmemo) - 31]);
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s", tmpmemo);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 9) {
    if (strlen(tmpdownload) + strlen(data) > 200) {
      return;
    }
    strcat(tmpdownload, data);
    if (strlen(tmpdownload) >= 31) {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<%s", &tmpdownload[strlen(tmpdownload) - 31]);
    } else {
      snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H %s", tmpdownload);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
  } else if (state == 20) {
    bool result;
    if (del_prompt->process(data, &result)) {
      if (result == true) {
        do_remove();
        save();
      }
      delete del_prompt;
      state = 0;
      display();
    }
  }
}

void dialdir::remove() {
  if (state == 0) {
    if (entries.size() > 0) {
      del_prompt = new yesno(t, entries.at(cur_item).name);
      del_prompt->draw();
      state = 20;
    }
  }
}

void dialdir::pgup() {
  if (state == 0) {
    searchdata[0] = '\0';
    if (entries.size() == 0) {
      return;
    }
    cur_item -= 11;
    if (cur_item < 0) {
      cur_item = 0;
    }
    display();
  }
}

void dialdir::left() {
  if (state == 20) {
    del_prompt->tab();
  }
}

void dialdir::right() {
  if (state == 20) {
    del_prompt->tab();
  }
}

void dialdir::up() {
  if (state == 0) {
    searchdata[0] = '\0';
    if (entries.size() == 0) {
      return;
    }
    cur_item--;
    if (cur_item < 0) {
      cur_item = entries.size() - 1;
    }
    display();
  } else if (state < 11) {
    state = state - 1;
    if (state == 0) {
      state = 10;
    }
    updateCursor();
  } else if (state == 20) {
    del_prompt->tab();
  }
}

void dialdir::down() {
  if (state == 0) {
    searchdata[0] = '\0';
    if (entries.size() == 0) {
      return;
    }
    cur_item++;
    if (cur_item == entries.size()) {
      cur_item = 0;
    }
    display();
  } else if (state < 11) {
    state = state + 1;
    if (state == 11) {
      state = 1;
    }
    updateCursor();
  } else if (state == 20) {
    del_prompt->tab();
  }
}

void dialdir::pgdown() {
  if (state == 0) {
    searchdata[0] = '\0';
    if (entries.size() == 0) {
      return;
    }
    cur_item += 11;
    if (cur_item >= entries.size()) {
      cur_item = entries.size() - 1;
    }
    display();
  }
}

void dialdir::tab() {
  if (state > 0 && state < 11) {
    state = state + 1;
    if (state == 11) {
      state = 1;
    }
    updateCursor();
  } else if (state == 20) {
    del_prompt->tab();
  } else if (state == 0) {
    display_memo = !display_memo;
    display();
  }
}

void dialdir::edit() {
  char buffer[256];
  char *ptr;
  int i;

  if (entries.size() == 0 || state != 0) {
    return;
  }

  editing = cur_item;
  tmptype = entries.at(editing).type;
  snprintf(buffer, 256, "\x1b[7;20H\x1b[0;46;30m+--------------------------------------+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[8;20H\x1b[0;46;30m| BBS Name:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[9;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[10;20H\x1b[0;46;30m| Hostname:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[11;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[12;20H\x1b[0;46;30m| Port:                                |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[13;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |", (tmptype == 0 ? 'X' : ' '), (tmptype == 1 ? 'X' : ' '));
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[15;20H\x1b[0;46;30m| Username:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[16;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[17;20H\x1b[0;46;30m| Password:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[19;20H\x1b[0;46;30m| Memo:                                |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[20;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[21;20H\x1b[0;46;30m| Download Directory:                  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[22;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[23;20H\x1b[0;46;30m+---------------------------[ OK ]-----+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  strncpy(tmpbbs, entries.at(editing).name.c_str(), 200);
  strncpy(tmphost, entries.at(editing).address.c_str(), 200);
  if (entries.at(editing).downloads != "") {
    strncpy(tmpdownload, entries.at(editing).downloads.c_str(), 1024);
  } else {
    memset(tmpdownload, 0, 1024);
  }
  tmpport = entries.at(editing).port;
  if (entries.at(editing).username != "") {
    strncpy(tmpuser, entries.at(editing).username.c_str(), 200);
  } else {
    memset(tmpuser, 0, 200);
  }
  if (entries.at(editing).memo != "") {
    strncpy(tmpmemo, entries.at(editing).memo.c_str(), 200);
  } else {
    memset(tmpmemo, 0, 200);
  }

  ptr = getpass(editing);

  if (ptr != NULL) {
    strcpy(tmppass, ptr);
    free(ptr);
  } else {
    memset(tmppass, 0, 200);
  }

  if (strlen(tmpbbs) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H<%s", &tmpbbs[strlen(tmpbbs) - 31]);
  } else {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;23H %s", tmpbbs);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));
  if (strlen(tmphost) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H<%s", &tmphost[strlen(tmphost) - 31]);
  } else {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[11;23H %s", tmphost);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));
  if (tmpport == 0) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H ");
  } else {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H %d", tmpport);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));

  if (strlen(tmpuser) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H<%s", &tmpuser[strlen(tmpuser) - 31]);
  } else if (strlen(tmpuser) > 0) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[16;23H %s", tmpuser);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));

  if (strlen(tmppass) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H<");
    ptr = &buffer[strlen(buffer)];
    for (i = 0; i < strlen(tmppass); i++) {
      *ptr++ = '*';
      *ptr = '\0';
    }
  } else {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[18;23H ");
    ptr = &buffer[strlen(buffer)];
    for (i = 0; i < strlen(tmppass); i++) {
      *ptr++ = '*';
      *ptr = '\0';
    }
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));

  if (strlen(tmpmemo) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s", &tmpmemo[strlen(tmpmemo) - 31]);
  } else if (strlen(tmpmemo) > 0) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s", tmpmemo);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));

  if (strlen(tmpdownload) >= 31) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<%s", &tmpdownload[strlen(tmpdownload) - 31]);
  } else if (strlen(tmpdownload) > 0) {
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H %s", tmpdownload);
  }
  t->twrite((unsigned char *)buffer, strlen(buffer));

  state = 1;
  updateCursor();
}

void dialdir::updateCursor() {
  char buffer[41];
  int plen;

  if (state == 10) {
    snprintf(buffer, 40, "\x1b[23;48H\x1b[0;40;37m[ OK ]\x1b[0;46;37m");
  } else {
    snprintf(buffer, 40, "\x1b[23;48H\x1b[0;46;30m[ OK ]\x1b[0;46;37m");
  }

  t->twrite((unsigned char *)buffer, strlen(buffer));

  if (state == 1) {
    if (strlen(tmpbbs) >= 31) {
      snprintf(buffer, 10, "\x1b[9;55H");
    } else {
      snprintf(buffer, 10, "\x1b[9;%uH", (unsigned long)strlen(tmpbbs) + 24);
    }
  } else if (state == 2) {
    if (strlen(tmphost) >= 31) {
      snprintf(buffer, 10, "\x1b[11;55H");
    } else {
      snprintf(buffer, 10, "\x1b[11;%uH", (unsigned long)strlen(tmphost) + 24);
    }
  } else if (state == 3) {
    if (tmpport > 0) {
      snprintf(buffer, 10, "%d", tmpport);
      plen = strlen(buffer);
      snprintf(buffer, 10, "\x1b[13;%dH", plen + 24);
    } else {
      snprintf(buffer, 10, "\x1b[13;24H");
    }
  } else if (state == 4) {
    snprintf(buffer, 10, "\x1b[14;24H");
  } else if (state == 5) {
    snprintf(buffer, 10, "\x1b[14;33H");
  } else if (state == 6) {
    if (strlen(tmpuser) >= 31) {
      snprintf(buffer, 10, "\x1b[16;55H");
    } else {
      snprintf(buffer, 10, "\x1b[16;%uH", (unsigned long)strlen(tmpuser) + 24);
    }
  } else if (state == 7) {
    if (strlen(tmppass) >= 31) {
      snprintf(buffer, 10, "\x1b[18;55H");
    } else {
      snprintf(buffer, 10, "\x1b[18;%uH", (unsigned long)strlen(tmppass) + 24);
    }
  } else if (state == 8) {
    if (strlen(tmpmemo) >= 31) {
      snprintf(buffer, 10, "\x1b[20;55H");
    } else {
      snprintf(buffer, 10, "\x1b[20;%uH", (unsigned long)strlen(tmpmemo) + 24);
    }
  } else if (state == 9) {
    if (strlen(tmpdownload) >= 31) {
      snprintf(buffer, 10, "\x1b[22;55H");
    } else {
      snprintf(buffer, 10, "\x1b[22;%uH", (unsigned long)strlen(tmpdownload) + 24);
    }
  } else if (state == 10) {
    snprintf(buffer, 10, "\x1b[23;52H");
  }

  t->twrite((unsigned char *)buffer, strlen(buffer));
}

void dialdir::insert() {
  char buffer[256];
  if (state != 0) {
    return;
  }
  editing = -1;
  snprintf(buffer, 256, "\x1b[7;20H\x1b[0;46;30m+--------------------------------------+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[8;20H\x1b[0;46;30m| BBS Name:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[9;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[10;20H\x1b[0;46;30m| Hostname:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[11;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[12;20H\x1b[0;46;30m| Port:                                |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[13;20H\x1b[0;46;30m|  \x1b[0;47;30m 22                               \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m|  [X] SSH  [ ] Telnet                 |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[15;20H\x1b[0;46;30m| Username:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[16;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[17;20H\x1b[0;46;30m| Password:                            |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[19;20H\x1b[0;46;30m| Memo:                                |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[20;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[21;20H\x1b[0;46;30m| Download Directory:                  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[22;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[23;20H\x1b[0;46;30m+---------------------------[ OK ]-----+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[0;47;30m\x1b[9;24H");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  memset(tmpbbs, 0, 200);
  memset(tmphost, 0, 200);
  memset(tmpuser, 0, 200);
  memset(tmppass, 0, 200);
  memset(tmpmemo, 0, 200);
  memset(tmpdownload, 0, 1024);
  tmpport = 22;
  state = 1;
  tmptype = 0;
}

const char *dialdir::getname() { return entries.at(cur_item).name.c_str(); }

const char *dialdir::gethost() { return entries.at(cur_item).address.c_str(); }

int dialdir::getport() { return entries.at(cur_item).port; }

const char *dialdir::getuser() { return entries.at(cur_item).username.c_str(); }

int dialdir::gettype() { return entries.at(cur_item).type; }

std::string dialdir::getdownloadpath() {
  if (entries.at(cur_item).downloads != "") {
    return entries.at(cur_item).downloads;
  } else {
    return default_download_directory;
  }
}

void dialdir::escape() {
  if (state == 0) {
    searchdata[0] = '\0';
    t->twrite("\x1b[0;46;30m\x1b[" + std::to_string(term_height) + ";30H\x1b[K");
  } else if (state == 20) {
    delete del_prompt;
    state = 0;
    display();
  } else {
    state = 0;
    display();
  }
}

void dialdir::do_remove() {
  if (entries.size() == 0) {
    return;
  }

  if (entries.at(cur_item).username != "") {
    wipepassword(cur_item, entries.at(cur_item).username.c_str());
  }
  entries.erase(entries.begin() + cur_item);
  cur_item--;
  if (cur_item < 0) {
    cur_item = 0;
  }
}

dialdir::~dialdir() {}
