#ifndef _MSC_VER
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#else
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <direct.h>
#endif
#include <string>
#include <sys/stat.h>
#include "filechooser.h"

#ifdef _MSC_VER
#define PATH_SEP '\\'
#define PATH_MAX MAX_PATH
#else
#define PATH_SEP '/'
#endif

filechooser::filechooser(Terminal *term) {
  t = term;
  dir_count = 0;
  file_count = 0;
  dir_top = 0;
  file_top = 0;
  dir_at = 0;
  file_at = 0;
  focus = 0;
#ifdef _MSC_VER
  _getcwd(current_path, PATH_MAX);
#else
  getcwd(current_path, PATH_MAX);
#endif
}

void filechooser::load() { load_dir(); }

void filechooser::draw() {
  draw_bg();
  draw_dirs();
  draw_files();
}

void filechooser::pgup() {
  if (focus == 0) {
    dir_at -= 9;
    if (dir_at < 0) {
      dir_at = 0;
    }

    if (dir_at < dir_top) {
      dir_top = dir_at;
    }
  } else {
    file_at -= 9;
    if (file_at < 0) {
      file_at = 0;
    }

    if (file_at < file_top) {
      file_top = file_at;
    }
  }
}

void filechooser::up() {
  if (focus == 0) {
    dir_at--;
    if (dir_at < 0) {
      dir_at = 0;
    }

    if (dir_at < dir_top) {
      dir_top = dir_at;
    }
  } else {
    file_at--;
    if (file_at < 0) {
      file_at = 0;
    }

    if (file_at < file_top) {
      file_top = file_at;
    }
  }
}

void filechooser::pgdown() {
  if (focus == 0) {
    dir_at += 9;
    if (dir_at >= dir_count) {
      dir_at = dir_count - 1;
    }
    if (dir_at > dir_top + 9) {
      dir_top = dir_at - 9;
    }
  } else {
    file_at += 9;
    if (file_at >= file_count) {
      file_at = file_count - 1;
    }
    if (file_at > file_top + 9) {
      file_top = file_at - 9;
    }
  }
}

void filechooser::down() {
  if (focus == 0) {
    dir_at++;
    if (dir_at >= dir_count) {
      dir_at = dir_count - 1;
    }
    if (dir_at > dir_top + 9) {
      dir_top = dir_at - 9;
    }
  } else {
    file_at++;
    if (file_at >= file_count) {
      file_at = file_count - 1;
    }
    if (file_at > file_top + 9) {
      file_top = file_at - 9;
    }
  }
}

void filechooser::tab() {
  if (file_count != 0) {
    focus = !focus;
  }
}

char *filechooser::enter() {
  char *tmp;
  int i;
  char old_cur_path[PATH_MAX];

  if (!focus) {
    strcpy(old_cur_path, current_path);
    if (strcmp(dirs[dir_at], "..") == 0) {
      for (i = strlen(current_path) - 1; i > 0; i--) {
        if (current_path[i] == PATH_SEP) {
          current_path[i] = '\0';
          break;
        }
      }
      if (i == 0) {
        sprintf(current_path, "%c", PATH_SEP);
      }
    } else {
      if (strcmp(current_path, "/") == 0) {
        snprintf(current_path, PATH_MAX, "%c%s", PATH_SEP, dirs[dir_at]);
      } else if (strcmp(current_path, "\\") == 0) {
        snprintf(current_path, PATH_MAX, "%s", dirs[dir_at]);
      } else {
        tmp = strdup(current_path);
        if (!tmp) {
          return NULL;
        }
        snprintf(current_path, PATH_MAX, "%s%c%s", tmp, PATH_SEP, dirs[dir_at]);
        free(tmp);
      }
    }
    if (load_dir() != 0) {
      strcpy(current_path, old_cur_path);
    }
    return NULL;
  } else {
    if (file_count == 0) {
      return NULL;
    }
    tmp = (char *)malloc(strlen(current_path) + strlen(files[file_at]) + 2);
    if (!tmp) {
      return NULL;
    }
    snprintf(tmp, PATH_MAX, "%s%c%s", current_path, PATH_SEP, files[file_at]);

    return tmp;
  }
}

void filechooser::draw_bg() {
  char buffer[256];

  snprintf(buffer, 256, "\x1b[0;46;30m\x1b[3;5H+--------------------------------------------------------------------+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[4;5H|  Choose File:                                                      |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[5;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[6;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[7;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[8;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[9;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[10;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[11;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[12;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[13;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[14;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[15;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[16;5H|                                                                    |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[17;5H|   Tab - Switch between Columns      Enter - Select                 |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[18;5H|   ESC - Cancel                                                     |");
  t->twrite((unsigned char *)buffer, strlen(buffer));
  snprintf(buffer, 256, "\x1b[19;5H+--------------------------------------------------------------------+");
  t->twrite((unsigned char *)buffer, strlen(buffer));
}

void filechooser::draw_dirs() {
  char buffer[256];
  int i;

  for (i = dir_top; i < dir_top + 10; i++) {
    if (i >= dir_count) {
      snprintf(buffer, 256, "\x1b[0m\x1b[%d;7H                                ", i - dir_top + 6);
      t->twrite((unsigned char *)buffer, strlen(buffer));
    } else {
      if (i == dir_at) {
        if (!focus) {
          snprintf(buffer, 256, "\x1b[1;36;43m\x1b[%d;7H%-32.32s", i - dir_top + 6, dirs[i]);
        } else {
          snprintf(buffer, 256, "\x1b[1;36;40m\x1b[%d;7H%-32.32s", i - dir_top + 6, dirs[i]);
        }
      } else {
        snprintf(buffer, 256, "\x1b[0m\x1b[%d;7H%-32.32s", i - dir_top + 6, dirs[i]);
      }

      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  }
}

void filechooser::draw_files() {
  char buffer[256];
  int i;

  for (i = file_top; i < file_top + 10; i++) {
    if (i >= file_count) {
      snprintf(buffer, 256, "\x1b[0m\x1b[%d;41H                                ", i - file_top + 6);
      t->twrite((unsigned char *)buffer, strlen(buffer));
    } else {
      if (i == file_at) {
        if (focus) {
          snprintf(buffer, 256, "\x1b[1;36;43m\x1b[%d;41H%-32.32s", i - file_top + 6, files[i]);
        } else {
          snprintf(buffer, 256, "\x1b[1;36;40m\x1b[%d;41H%-32.32s", i - file_top + 6, files[i]);
        }
      } else {
        snprintf(buffer, 256, "\x1b[0m\x1b[%d;41H%-32.32s", i - file_top + 6, files[i]);
      }

      t->twrite((unsigned char *)buffer, strlen(buffer));
    }
  }
}

static int strCompare(const void *a, const void *b) { return strcmp(*(const char **)a, *(const char **)b); }

int filechooser::load_dir() {
#ifdef _MSC_VER
  HANDLE hFind;
  WIN32_FIND_DATA FindFileData;
  char findfile[PATH_MAX];
#else
  DIR *dirp;
  struct dirent *dent;
#endif
  struct stat st;
  char filepath[PATH_MAX];
  int i;

#ifdef _MSC_VER
  DWORD maxcount = PATH_MAX;
  DWORD count;
  char drivebuffer[PATH_MAX];
  char *drive;
  if (stricmp(current_path, "\\") == 0) {
    count = GetLogicalDriveStrings(maxcount, drivebuffer);
    if (dir_count != 0) {
      for (i = 0; i < dir_count; i++) {
        free(dirs[i]);
      }
      free(dirs);
      dir_count = 0;
    }

    if (file_count != 0) {
      for (i = 0; i < file_count; i++) {
        free(files[i]);
      }
      free(files);
      file_count = 0;
    }
    if (count > 0 && count < PATH_MAX) {
      drive = drivebuffer;
      while (*drive) {
        if (dir_count == 0) {
          dirs = (char **)malloc(sizeof(char *));
        } else {
          dirs = (char **)realloc(dirs, sizeof(char *) * (dir_count + 1));
        }
        if (!dirs) {
          return -1;
        }
        dirs[dir_count] = strdup(drive);
        if (!dirs[dir_count]) {
          return -1;
        }
        dirs[dir_count][strlen(dirs[dir_count]) - 1] = '\0';
        drive = drive + strlen(drive) + 1;
        dir_count++;
      }
    }
    return 0;
  } else {
    snprintf(findfile, PATH_MAX, "%s%c*", current_path, PATH_SEP);
  }
  hFind = FindFirstFile(findfile, &FindFileData);
  if (hFind == INVALID_HANDLE_VALUE) {
    return -1;
  }
#else
  dirp = opendir(current_path);
  if (!dirp) {
    return -1;
  }
#endif

  if (dir_count != 0) {
    for (i = 0; i < dir_count; i++) {
      free(dirs[i]);
    }
    free(dirs);
    dir_count = 0;
  }

  if (file_count != 0) {
    for (i = 0; i < file_count; i++) {
      free(files[i]);
    }
    free(files);
    file_count = 0;
  }

  dirs = (char **)malloc(sizeof(char *));
  dirs[0] = strdup("..");
  dir_count = 1;

  dir_at = 0;
  file_at = 0;
  dir_top = 0;
  file_top = 0;

#ifdef _MSC_VER
  do {
#else
  while ((dent = readdir(dirp)) != NULL) {
#endif
#ifdef _MSC_VER
    if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) && FindFileData.cFileName[0] != '.') {
      if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
#else
    snprintf(filepath, PATH_MAX, "%s%c%s", current_path, PATH_SEP, dent->d_name);
    if (stat(filepath, &st) == 0) {
      if (dent->d_name[0] != '.') {
        if (S_ISDIR(st.st_mode)) {
#endif
        dirs = (char **)realloc(dirs, sizeof(char *) * (dir_count + 1));
        if (dirs == NULL) {
          return -1;
        }
#ifdef _MSC_VER
        dirs[dir_count] = strdup(FindFileData.cFileName);
#else
          dirs[dir_count] = strdup(dent->d_name);
#endif
        if (dirs[dir_count] == NULL) {
          return -1;
        }
        dir_count++;
      }
#ifdef _MSC_VER
      else {
#else
        else if (S_ISREG(st.st_mode)) {
#endif
        if (file_count == 0) {
          files = (char **)malloc(sizeof(char *));
        } else {
          files = (char **)realloc(files, sizeof(char *) * (file_count + 1));
        }
        if (files == NULL) {
          return -1;
        }
#ifdef _MSC_VER
        files[file_count] = strdup(FindFileData.cFileName);
#else
          files[file_count] = strdup(dent->d_name);
#endif
        if (files[file_count] == NULL) {
          return -1;
        }
        file_count++;
      }
    }

#ifdef _MSC_VER
  } while (FindNextFile(hFind, &FindFileData));
  FindClose(hFind);
#else
    }
  }
  closedir(dirp);
#endif

  qsort(&dirs[1], dir_count - 1, sizeof(char *), strCompare);
  qsort(files, file_count, sizeof(char *), strCompare);

  return 0;
}

filechooser::~filechooser() {
  int i;

  for (i = 0; i < file_count; i++) {
    free(files[i]);
  }
  if (file_count > 0)
    free(files);

  for (i = 0; i < dir_count; i++) {
    free(dirs[i]);
  }
  if (dir_count > 0)
    free(dirs);
}
