#ifndef TERMINAL_H
#define TERMINAL_H

#ifdef _MSC_VER
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include <libssh/libssh.h>
#include <vector>
#include <string>
#include <sstream>

extern void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
extern Uint32 getpixel(SDL_Surface *surface, int x, int y);

struct screen_mode_t {
  int term_width;
  int term_height;

  int screen_width;
  int screen_height;
};

struct term_font {
  int type; // 1 = 8x16, 2 = 8x14, 3 = 8x8
  int slot;
  std::string data;
};

extern struct screen_mode_t smodes[];
class Terminal {
public:
  Terminal(SDL_Window *window, int smode);
  virtual ~Terminal();
  void twrite(std::string str);
  void twrite(unsigned char *c, int len);
  SDL_Surface *getSurface();
  void drawChar(unsigned char c);
  void setZmodem(int *z);
  char *gettext(int sx, int sy, int ex, int ey);
  void clickLink(int x, int y);
  void scrollback();
  void sb_up();
  void sb_down();
  void screenshot();
  void toggleIceColours();
  int isIceEnabled();
  void resize(int smode);
  int screen_mode;
  void set_download_path(std::string download_path);

protected:
private:
  std::string download_path;
  std::vector<int> tabstops;
  void scrollUp();
  SDL_Window *win;
  SDL_Surface *screen;
  SDL_Surface *font_bmp;
  SDL_Surface *pot_noodle_bmp;
  SDL_Surface *topaz_plus_bmp;
  SDL_Surface *mk_plus_bmp;
  SDL_Surface *mk_bmp;
  SDL_Surface *topaz_bmp;
  SDL_Surface *mosoul_bmp;
  SDL_Surface *screenbacker;
  SDL_Surface *screencopy;
  SDL_Surface *blink_screen;
  SDL_Surface *scrollback_screen;
  SDL_Surface *sixel_surf;
  Uint32 last_show;
  int color;
  int bgcolor;
  int cur_col;
  int cur_row;
  int state;
  int params[16];
  int param_count;
  int save_col;
  int save_row;
  int bold;
  int vt320_count;
  int vt320[16];
  int cursor;
  int zmodem_detect;
  int *zmodem;
  int curfont[4];
  unsigned char **screen_buffer;
  unsigned char **scrollback_buffer;
  int blinking;
  int blink_toggle;
  int high_intensity_bg;
  int high_intensity_bg_enabled;
  int blink_disabled;
  int scrollingBack;
  int scrollBackTop;
  int icecolours;
  Uint32 colours[16] = {0xff000000, 0xffaa0000, 0xff00aa00, 0xffaaaa00, 0xff0000aa, 0xffaa00aa, 0xff0055aa, 0xffaaaaaa,
                        0xff555555, 0xffff5555, 0xff55ff55, 0xffffff55, 0xff5555ff, 0xffff55ff, 0xff55ffff, 0xffffffff};
  Uint32 fg_colour;
  Uint32 bg_colour;
  std::stringstream dcs_string;
  void display_sixel(std::string data);
  std::vector<struct term_font> custom_fonts;
  bool gotcterm;
  bool gotcterm_equals;
  int prev_font_selection_result;
  int sixel_surf_at;
  float sixel_scale;
  int margin_top;
  int margin_bottom;
};

#endif // TERMINAL_H
