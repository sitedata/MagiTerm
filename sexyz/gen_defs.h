#ifndef __GEN_DEFS_H__
#define __GEN_DEFS_H__

#ifndef _MSC_VER
#ifndef BOOL
typedef unsigned char BOOL;
#endif
#endif
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef _MSC_VER
#define MAX_PATH PATH_MAX
#define SOCKET int
#define INVALID_SOCKET -1
#endif

#ifndef INT_TO_BOOL
#define INT_TO_BOOL(x) ((x) ? TRUE : FALSE)
#endif

#define LOG_ERR 4
#define LOG_WARNING 3
#define LOG_NOTICE 2
#define LOG_INFO 1
#define LOG_DEBUG 0
#define SAFECOPY(dst, src) sprintf(dst, "%.*s", (int)sizeof(dst) - 1, src)
#endif