#ifndef __DIALDIRECTORY_H_
#define __DIALDIRECTORY_H_

#include <vector>
#include <string>
#include "Terminal.h"

class yesno;

struct dialinfo {
  std::string name;
  std::string address;
  int port;
  std::string username;
  std::string memo;
  std::string downloads;
  int type;
  bool pinned;
};

class dialdir {
public:
  dialdir(Terminal *term, int width, int height);
  ~dialdir();
  void process(char *data);
  void up();
  void pgup();
  void pgdown();
  void down();
  void insert();
  void backspace();
  int enter();
  void edit();
  void tab();
  const char *gethost();
  int getport();
  const char *getname();
  const char *getuser();
  std::string getdownloadpath();
  char *getpass(int item);
  int gettype();
  void savepass(int entry, const char *username, const char *password);
  void escape();
  void save();
  void display();
  void load();
  void remove();
  void sort();
  void left();
  void right();
  void pin_entry();
  void doexport(char *exportpath);
  void doimport(char *importpath);
  void resize(int width, int height);

private:
  std::string default_download_directory;
  bool display_memo = false;
  std::vector<struct dialinfo> entries;
  void do_remove();
  yesno *del_prompt;
  int new_width;
  int new_height;
  int term_width;
  int term_height;
  Terminal *t;
  int cur_item;
  int state;
  char tmpbbs[200];
  char tmphost[200];
  int tmptype;
  int tmpport;
  char tmpuser[200];
  char tmppass[200];
  char tmpmemo[200];
  char tmpdownload[1024];
  int top;
  int editing;
  void updateCursor();
  void storepassword(int entry, const char *username, const char *password);
  int retrievepassword(int entry, const char *username, char **password);
  void wipepassword(int entry, const char *username);
  char searchdata[257];
};

#endif
