#include <string>
#include <sstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <regex>
#ifndef _MSC_VER
#include <pwd.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <libgen.h>
#if defined(__linux__)
#include <pty.h>
#include <utmp.h>
#else
#if defined(__FreeBSD__)
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <libutil.h>
#endif
#if defined(__OpenBSD__)
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <util.h>
#endif
#endif
#include <termios.h>
#include <signal.h>
#ifndef __APPLE__
#include <unistd.h>
#else
#include <CoreFoundation/CoreFoundation.h>
extern "C" {
extern void NSBeep();
}
#include <util.h>
#include <sys/ioctl.h>
#include <libgen.h>
#endif
#ifdef __sun
#include <sys/file.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <unistd.h>
#include <libgen.h>
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <SDL_image.h>
#else
#include <SDL.h>
#include <SDL_image.h>
#define WIN32_LEAN_AND_MEAN 1
#include <Shlobj.h>
#include <shellapi.h>
#include <direct.h>
#include <vector>
#include <string>
#define PATH_MAX MAX_PATH
#endif
#include "Terminal.h"
#include "fonts/cp437.h"
#include "fonts/potnoodle.h"
#include "fonts/topazplus.h"
#include "fonts/mkplus.h"
#include "fonts/microknight.h"
#include "fonts/topaz.h"
#include "fonts/mosoul.h"
extern "C" {
#include "sexyz/zmodem.h"
}
#define IAC 255
#define IAC_WILL 251
#define IAC_WONT 252
#define IAC_DO 253
#define IAC_DONT 254
#define IAC_TRANSMIT_BINARY 0
#define IAC_SUPPRESS_GO_AHEAD 3
#define IAC_ECHO 1

#define IS 0
#define SEND 1
#define REPLY 2
#define NAME 3

#define TERMINAL_TYPE 24
#define NAWS 31
#define TERMINAL_SPEED 32
#define REMOTE_FLOW_CONTROL 33

#ifdef _MSC_VER
#define PATH_SEP '\\'
#else
#define PATH_SEP '/'
#endif

extern "C" int zm_update;
extern "C" int zm_type;
extern "C" int zm_value;
extern "C" char *zm_status;

extern int type;
extern int tsock;
extern ssh_channel chan;
extern SDL_Thread *dlthread;

extern int rz;
extern int sz;
extern int ft;

int echo = 0;

struct zmodem_info {
  int *ptr;
  char *uploadfile;
  const char *download_path;
};

struct zmodem_info zm_inf;

extern SDL_Surface *loadImageSurf(unsigned char *iaddr, int ilen);

int do_zmodem_download(void *ptr);
int do_zmodem_upload(void *ptr);

static std::string base64_decode(const std::string &in) {

  std::string out;

  std::vector<int> T(256, -1);
  for (int i = 0; i < 64; i++)
    T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;

  int val = 0, valb = -8;
  for (unsigned char c : in) {
    if (T[c] == -1)
      break;
    val = (val << 6) + T[c];
    valb += 6;
    if (valb >= 0) {
      out.push_back(char((val >> valb) & 0xFF));
      valb -= 8;
    }
  }
  return out;
}

int send_telnet(int sock, const char *buff, int len, int flags) {
  int i;
  for (i = 0; i < len; i++) {
    if ((unsigned char)buff[i] == IAC) {
      send(sock, &buff[i], 1, flags);
    }
    send(sock, &buff[i], 1, flags);
  }
  return i;
}

static int gotTermType = 0;
static int dosendterm = 0;
static int whichterm = 0;
static int gotNaws = 0;
static int dosendnaws = 0;

unsigned char responses[256] = {0};

extern Terminal *term;

int recv_telnet(int sock, char *buff, int len, int flags) {
  int i;
  unsigned char c = 0;
  unsigned char buf[15];
  int count;
  static int gotIAC = 0;
  static unsigned char iacc;
  i = 0;

  while (i < len) {
    count = recv(sock, (char *)&c, 1, flags);
    if (count <= 0) {
      if (i > 0) {
        return i;
      }
      return count;
    }

    if (c == IAC) {
      if (gotIAC == 1) {
        buff[i++] = c;
        gotIAC = 0;
      } else if (gotIAC != 3) {
        gotIAC = 1;
      }
    } else {
      if (gotIAC != 0) {
        if (gotIAC == 1) {
          if (c == 250) {
            gotIAC = 3;
          } else {
            if (c == IAC_WILL || c == IAC_WONT || c == IAC_DO || c == IAC_DONT) {
              iacc = c;
              gotIAC = 2;
            } else {
              gotIAC = 0;
            }
          }
        } else if (gotIAC == 2) {

          if (iacc == IAC_WILL) {
            if (responses[c] != IAC_WILL && responses[c] != IAC_WONT) {
              buf[0] = IAC;
              if (c == IAC_ECHO || c == IAC_SUPPRESS_GO_AHEAD || c == IAC_TRANSMIT_BINARY || c == TERMINAL_TYPE || c == NAWS) {
                buf[1] = IAC_DO;
                responses[c] = IAC_WILL;
              } else {
                buf[1] = IAC_DONT;
                responses[c] = IAC_WONT;
              }
              buf[2] = c;
              send(sock, (char *)buf, 3, 0);
            }
          } else if (iacc == IAC_WONT) {
            if (responses[c] != IAC_WONT) {
              buf[0] = IAC;
              buf[1] = IAC_DONT;
              buf[2] = c;

              send(sock, (char *)buf, 3, 0);
              responses[c] = IAC_WONT;
            }
          } else if (iacc == IAC_DO) {
            if (c == NAWS || c == IAC_ECHO || c == IAC_SUPPRESS_GO_AHEAD || c == IAC_TRANSMIT_BINARY || c == TERMINAL_TYPE) {
              if (responses[c] != IAC_DO) {
                buf[0] = IAC;
                buf[1] = IAC_WILL;
                buf[2] = c;
                send(sock, (char *)buf, 3, 0);
                responses[c] = IAC_DO;
              }
            } else {
              if (responses[c] != IAC_DO) {
                buf[0] = IAC;
                buf[1] = IAC_WONT;
                buf[2] = c;
                send(sock, (char *)buf, 3, 0);
                responses[c] = IAC_DO;
              }
            }

          } else if (iacc == IAC_DONT) {
            if (responses[c] != IAC_DONT) {
              buf[0] = IAC;
              buf[1] = IAC_WONT;
              buf[2] = c;
              send(sock, (char *)buf, 3, 0);
              responses[c] = IAC_DONT;
            }
          }

          gotIAC = 0;
        } else if (gotIAC == 3) {

          if (c == TERMINAL_TYPE) {
            gotTermType = 1;
          } else if (c == NAWS) {
            gotNaws = 1;
          } else if (c == SEND) {
            if (gotTermType == 1) {
              dosendterm = 1;
              gotTermType = 0;
            } else if (gotNaws == 1) {
              dosendnaws = 1;
              gotNaws = 0;
            }
          } else if (c == 240) {
            if (dosendnaws == 1) {
              buf[0] = IAC;
              buf[1] = 250;
              buf[2] = NAWS;
              buf[3] = 0;
              buf[4] = smodes[term->screen_mode].term_width;
              buf[5] = 0;
              buf[6] = smodes[term->screen_mode].term_height;
              buf[7] = IAC;
              buf[8] = 240;
              send(sock, (char *)buf, 9, 0);
              dosendnaws = 0;
            } else if (dosendterm == 1) {
              if (whichterm == 0) {
                buf[0] = IAC;
                buf[1] = 250;
                buf[2] = TERMINAL_TYPE;
                buf[3] = IS;
                buf[4] = 'm';
                buf[5] = 'a';
                buf[6] = 'g';
                buf[7] = 'i';
                buf[8] = 't';
                buf[9] = 'e';
                buf[10] = 'r';
                buf[11] = 'm';

                buf[12] = IAC;
                buf[13] = 240;
                send(sock, (char *)buf, 14, 0);
                whichterm++;
              } else if (whichterm == 1) {
                buf[0] = IAC;
                buf[1] = 250;
                buf[2] = TERMINAL_TYPE;
                buf[3] = IS;
                buf[4] = 'a';
                buf[5] = 'n';
                buf[6] = 's';
                buf[7] = 'i';
                buf[8] = '-';
                buf[9] = 'b';
                buf[10] = 'b';
                buf[11] = 's';

                buf[12] = IAC;
                buf[13] = 240;
                send(sock, (char *)buf, 14, 0);
                whichterm++;
              } else if (whichterm == 2) {
                buf[0] = IAC;
                buf[1] = 250;
                buf[2] = TERMINAL_TYPE;
                buf[3] = IS;
                buf[4] = 'u';
                buf[5] = 'n';
                buf[6] = 'k';
                buf[7] = 'n';
                buf[8] = 'o';
                buf[9] = 'w';
                buf[10] = 'n';

                buf[11] = IAC;
                buf[12] = 240;
                send(sock, (char *)buf, 13, 0);
              }
              dosendterm = 0;
            }
            gotIAC = 0;
          }
        }
      } else {
        buff[i++] = c;
      }
    }
  }

  return i;
}

#ifdef __sun
int openpty(int *amaster, int *aslave, char *name, void *termp, void *winp) {
  int ptm;
  char *pname;
  int pts;

  ptm = open("/dev/ptmx", O_RDWR);

  grantpt(ptm);
  unlockpt(ptm);

  pname = ptsname(ptm);

  if (name != NULL) {
    strcpy(name, pname);
  }

  pts = open(pname, O_RDWR);
  ioctl(pts, I_PUSH, "ptem");
  ioctl(pts, I_PUSH, "ldterm");
  ioctl(pts, I_PUSH, "ttcompat");

  if (termp != NULL) {
    tcsetattr(pts, TCSAFLUSH, (const termios *)termp);
  }
  if (winp != NULL) {
    ioctl(pts, TIOCSWINSZ, winp);
  }

  *amaster = ptm;
  *aslave = pts;

  return 0;
}
#endif

void zmodem_upload(char *filename) {
  unsigned char buf[3];

  if (zm_inf.uploadfile != NULL) {
    free(zm_inf.uploadfile);
  }

  zm_inf.uploadfile = strdup(filename);

  if (type == 1) {
    buf[0] = IAC;
    buf[1] = IAC_WILL;
    buf[2] = IAC_TRANSMIT_BINARY;
    send(tsock, (char *)buf, 3, 0);
  }

  dlthread = SDL_CreateThread(do_zmodem_upload, "ZMODEM", &zm_inf);
}

#ifndef _MSC_VER
int ttySetRaw(int fd, struct termios *prevTermios) {
  struct termios t;

  if (tcgetattr(fd, &t) == -1)
    return -1;

  if (prevTermios != NULL)
    *prevTermios = t;

  t.c_lflag &= ~(ICANON | ISIG | IEXTEN | ECHO);
  t.c_iflag &= ~(BRKINT | ICRNL | IGNBRK | IGNCR | INLCR | INPCK | ISTRIP | IXON | PARMRK);
  t.c_oflag &= ~OPOST;
  t.c_cc[VMIN] = 1;
  t.c_cc[VTIME] = 0;

  if (tcsetattr(fd, TCSAFLUSH, &t) == -1)
    return -1;

  return 0;
}
#endif

void Terminal::resize(int smode) {
  Uint32 rmask, gmask, bmask, amask;
  int i, j;
  SDL_Rect src_rect;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif
  SDL_Surface *screenbacker2, *blink_screen2;

  src_rect.x = 0;
  src_rect.y = 0;
  src_rect.w = smodes[screen_mode].screen_width;
  src_rect.h = smodes[screen_mode].screen_height;

  for (i = 0; i < smodes[screen_mode].term_width; i++) {
    free(scrollback_buffer[i]);
    free(screen_buffer[i]);
  }
  screenbacker2 = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[smode].screen_width, smodes[smode].screen_height, 32, rmask, gmask, bmask, amask);
  blink_screen2 = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[smode].screen_width, smodes[smode].screen_height, 32, rmask, gmask, bmask, amask);

  SDL_BlitSurface(screenbacker, &src_rect, screenbacker2, &src_rect);
  SDL_BlitSurface(blink_screen, &src_rect, blink_screen2, &src_rect);

  free(scrollback_buffer);
  free(screen_buffer);
  SDL_FreeSurface(screen);
  SDL_FreeSurface(screenbacker);
  SDL_FreeSurface(screencopy);
  SDL_FreeSurface(blink_screen);
  SDL_FreeSurface(scrollback_screen);

  screen_mode = smode;

  screen = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  blink_screen = blink_screen2;
  screencopy = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  screenbacker = screenbacker2;
  scrollback_screen = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);

  screen_buffer = (unsigned char **)malloc(smodes[screen_mode].term_width * (sizeof(unsigned char *)));
  scrollback_buffer = (unsigned char **)malloc(smodes[screen_mode].term_width * (sizeof(unsigned char *)));
  for (i = 0; i < smodes[screen_mode].term_width; i++) {
    screen_buffer[i] = (unsigned char *)malloc(smodes[screen_mode].term_height * (sizeof(unsigned char)));
    for (j = 0; j < smodes[screen_mode].term_height; j++) {
      screen_buffer[i][j] = ' ';
    }
    scrollback_buffer[i] = (unsigned char *)malloc(smodes[screen_mode].term_height * (sizeof(unsigned char) * 4));
    for (j = 0; j < smodes[screen_mode].term_height * 4; j++) {
      scrollback_buffer[i][j] = ' ';
    }
  }

  tabstops.clear();

  for (i = 1; i < smodes[screen_mode].term_width / 8; i++) {
    tabstops.push_back(i * 8 + 1);
  }
}

Terminal::Terminal(SDL_Window *window, int smode) {
  // ctor
  Uint32 rmask, gmask, bmask, amask;
  int i, j;

  this->screen_mode = smode;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif

  this->win = window;
  this->screen = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  this->screenbacker = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  this->screencopy = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  this->blink_screen = SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  this->scrollback_screen =
      SDL_CreateRGBSurface(SDL_SWSURFACE, smodes[screen_mode].screen_width, smodes[screen_mode].screen_height, 32, rmask, gmask, bmask, amask);
  this->font_bmp = loadImageSurf((unsigned char *)CP437_IMAGE_DATA, sizeof(CP437_IMAGE_DATA));
  this->pot_noodle_bmp = loadImageSurf((unsigned char *)POTNOODLE_IMAGE_DATA, sizeof(POTNOODLE_IMAGE_DATA));
  this->topaz_plus_bmp = loadImageSurf((unsigned char *)TOPAZPLUS_IMAGE_DATA, sizeof(TOPAZPLUS_IMAGE_DATA));
  this->mk_plus_bmp = loadImageSurf((unsigned char *)MKPLUS_IMAGE_DATA, sizeof(MKPLUS_IMAGE_DATA));
  this->mk_bmp = loadImageSurf((unsigned char *)MK_IMAGE_DATA, sizeof(MK_IMAGE_DATA));
  this->topaz_bmp = loadImageSurf((unsigned char *)TOPAZ_IMAGE_DATA, sizeof(TOPAZ_IMAGE_DATA));
  this->mosoul_bmp = loadImageSurf((unsigned char *)MOSOUL_IMAGE_DATA, sizeof(MOSOUL_IMAGE_DATA));
  this->sixel_surf = NULL;
  this->color = 7;
  this->bgcolor = 0;
  this->cur_col = 0;
  this->cur_row = 0;
  this->state = 0;
  this->bold = 0;
  this->save_col = 0;
  this->save_row = 0;
  this->vt320_count = 0;
  this->cursor = 1;
  this->zmodem_detect = 0;
  this->zmodem = NULL;
  this->curfont[0] = 0;
  this->curfont[1] = 0;
  this->curfont[2] = 0;
  this->curfont[3] = 0;
  this->blinking = 0;
  this->last_show = 0;
  this->blink_toggle = 0;
  this->high_intensity_bg = 0;
  this->high_intensity_bg_enabled = 0;
  this->blink_disabled = 0;
  this->scrollingBack = 0;
  this->scrollBackTop = 75;
  this->icecolours = 0;
  this->param_count = 0;
  this->fg_colour = this->colours[7];
  this->bg_colour = this->colours[0];
  this->gotcterm = false;
  this->gotcterm_equals = false;
  this->prev_font_selection_result = 99;
  this->sixel_surf_at = -1;
  this->sixel_scale = 1;
  chan = NULL;

  screen_buffer = (unsigned char **)malloc(smodes[screen_mode].term_width * (sizeof(unsigned char *)));
  scrollback_buffer = (unsigned char **)malloc(smodes[screen_mode].term_width * (sizeof(unsigned char *)));
  for (i = 0; i < smodes[screen_mode].term_width; i++) {
    screen_buffer[i] = (unsigned char *)malloc(smodes[screen_mode].term_height * (sizeof(unsigned char)));
    for (j = 0; j < smodes[screen_mode].term_height; j++) {
      screen_buffer[i][j] = ' ';
    }
    scrollback_buffer[i] = (unsigned char *)malloc(smodes[screen_mode].term_height * (sizeof(unsigned char) * 4));
    for (j = 0; j < smodes[screen_mode].term_height * 4; j++) {
      scrollback_buffer[i][j] = ' ';
    }
  }

  for (i = 1; i < smodes[screen_mode].term_width / 8; i++) {
    tabstops.push_back(i * 8 + 1);
  }
}

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to set */
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch (bpp) {
  case 1:
    *p = pixel;
    break;

  case 2:
    *(Uint16 *)p = pixel;
    break;

  case 3:
    if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
      p[0] = (pixel >> 16) & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = pixel & 0xff;
    } else {
      p[0] = pixel & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = (pixel >> 16) & 0xff;
    }
    break;

  case 4:
    *(Uint32 *)p = pixel;
    break;
  }
}

Uint32 getpixel(SDL_Surface *surface, int x, int y) {
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to retrieve */
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch (bpp) {
  case 1:
    return *p;
    break;

  case 2:
    return *(Uint16 *)p;
    break;

  case 3:
    if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
      return p[0] << 16 | p[1] << 8 | p[2];
    else
      return p[0] | p[1] << 8 | p[2] << 16;
    break;

  case 4:
    return *(Uint32 *)p;
    break;

  default:
    return 0; /* shouldn't happen, but avoids warnings */
  }
}

void Terminal::set_download_path(std::string dlpath) {
  download_path = dlpath;
  whichterm = 0;
}

struct sixel_pallet {
  int pentry;
  uint32_t colour;
};

void Terminal::display_sixel(std::string data) {
  int raster_attribs[4] = {0, 0, 0, 0};
  uint32_t rmask, gmask, bmask, amask;
  std::vector<struct sixel_pallet> pallet;

  uint32_t cur_colour = fg_colour;
  int x = 0;
  int y = 0;
  SDL_Surface *unscaled_sixel_surf = NULL;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif
  for (size_t i = 0; i < data.size(); i++) {
    switch (data.at(i)) {
    case '!':
      // Graphics repeater introducer
      {
        int rep = 0;
        uint8_t ch;
        for (size_t j = i + 1; j < data.size(); j++) {
          if (data.at(j) >= '0' && data.at(j) <= '9') {
            rep = rep * 10 + (data.at(j) - '0');
          } else {
            ch = data.at(j);
            i = j;
            break;
          }
        }

        for (size_t j = 0; j < rep; j++) {
          // draw ch rep times
          uint8_t pix = ch - 0x3f;
          for (int k = 0; k < 6; k++) {
            if (pix >> k & 0x1) {
              for (int x1 = 0; x1 < raster_attribs[1]; x1++) {
                for (int y1 = 0; y1 < raster_attribs[0]; y1++) {
                  if (x + x1 > raster_attribs[1] * raster_attribs[2] || y + ((y1 + 1) * k) > raster_attribs[0] * raster_attribs[3]) {
                    // printf("Pixel out of bounds x = %d, y = %d\n pan %d pitch %d width %d height %d\n", x + x1, y + y1 * k, raster_attribs[0],
                    // raster_attribs[1], raster_attribs[2], raster_attribs[3]);
                  } else {
                    putpixel(unscaled_sixel_surf, x + x1, y + ((y1 + 1) * k), cur_colour);
                  }
                }
              }
            }
          }
          x += raster_attribs[1];
        }
      }
      break;
    case '"':
      // Raster attributes
      {
        int rast_at = 0;
        for (size_t j = i + 1; j < data.size(); j++) {
          if (data.at(j) >= '0' && data.at(j) <= '9') {
            if (rast_at < 4) {
              raster_attribs[rast_at] = raster_attribs[rast_at] * 10 + (data.at(j) - '0');
            }
          } else if (data.at(j) == ';') {
            rast_at++;
          } else {
            i = j - 1;
            break;
          }
        }

        if (unscaled_sixel_surf != NULL) {
          SDL_FreeSurface(unscaled_sixel_surf);
          if (sixel_surf != NULL) {
            sixel_surf = NULL;
            sixel_surf_at = -1;
          }
        }
        unscaled_sixel_surf =
            SDL_CreateRGBSurface(SDL_SWSURFACE, raster_attribs[1] * raster_attribs[2], raster_attribs[0] * raster_attribs[3], 32, rmask, gmask, bmask, amask);
        SDL_LockSurface(unscaled_sixel_surf);
      }
      break;
    case '#':
      // Colour Select / pallet
      {
        int temp = 0;
        int pallet_tmp[5] = {0, 0, 0, 0, 0};
        int pallet_at = 0;
        for (size_t j = i + 1; j < data.size(); j++) {
          if (data.at(j) >= '0' && data.at(j) <= '9') {
            temp = temp * 10 + (data.at(j) - '0');
          } else if (data.at(j) == ';') {
            if (pallet_at < 5) {
              pallet_tmp[pallet_at] = temp;
            }
            pallet_at++;
            temp = 0;
          } else {
            if (pallet_at == 0) {
              // it's a colour change
              for (size_t k = 0; k < pallet.size(); k++) {
                if (pallet.at(k).pentry == temp) {
                  cur_colour = pallet.at(k).colour;
                  break;
                }
              }
            } else {
              pallet_tmp[pallet_at] = temp;
              // it's a pallet definition
              struct sixel_pallet p;
              p.pentry = pallet_tmp[0];

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
              p.colour = 0x000000ff | ((int)(255.f / 100.f * (float)pallet_tmp[4]) << 8) | ((int)(255.f / 100.f * (float)pallet_tmp[3]) << 16) |
                         ((int)(255.f / 100.f * (float)pallet_tmp[2]) << 24);
#else

              p.colour = 0xff000000 | ((int)(255.f / 100.f * (float)pallet_tmp[4]) << 16) | ((int)(255.f / 100.f * (float)pallet_tmp[3]) << 8) |
                         (int)(255.f / 100.f * (float)pallet_tmp[2]);
#endif
              bool found = false;
              for (size_t k = 0; k < pallet.size(); k++) {
                if (pallet.at(k).pentry == p.pentry) {
                  found = true;
                  // printf("P.entry %d found was %x now %x\n", p.pentry, pallet.at(k).colour, p.colour);
                  pallet.at(k).colour = p.colour;
                  break;
                }
              }
              if (!found) {
                pallet.push_back(p);
              }
            }
            i = j - 1;
            break;
          }
        }
      }
      break;
    case '$':
      // Graphics CR
      x = 0;
      break;
    case '-':
      // Graphics New Line
      x = 0;
      y += raster_attribs[0] * 6;
      break;
    default:
      // sixel pixel
      {
        uint8_t pix = (uint8_t)data.at(i) - 0x3f;
        for (int k = 0; k < 6; k++) {
          if (pix >> k & 0x1) {
            for (int x1 = 0; x1 < raster_attribs[1]; x1++) {
              for (int y1 = 0; y1 < raster_attribs[0]; y1++) {
                if (x + x1 > raster_attribs[1] * raster_attribs[2] || y + ((y1 + 1) * k) > raster_attribs[0] * raster_attribs[3]) {
                  // printf("Pixel out of bounds x = %d, y = %d\n pan %d pitch %d width %d height %d\n", x + x1, y + y1 * k, raster_attribs[0],
                  // raster_attribs[1], raster_attribs[2], raster_attribs[3]);
                } else {
                  putpixel(unscaled_sixel_surf, x + x1, y + ((y1 + 1) * k), cur_colour);
                }
              }
            }
          }
        }
        x += raster_attribs[1];
      }
      break;
    }
  }
  if (unscaled_sixel_surf != NULL) {
    SDL_UnlockSurface(unscaled_sixel_surf);
    SDL_Rect dest;

    if (unscaled_sixel_surf->w > smodes[screen_mode].screen_width) {
      sixel_scale = (float)smodes[screen_mode].screen_width / (float)unscaled_sixel_surf->w;
    } else {
      sixel_scale = 1.0;
    }

    if (sixel_surf != NULL) {
      SDL_FreeSurface(sixel_surf);
    }

    sixel_surf = SDL_CreateRGBSurface(SDL_SWSURFACE, (int)((float)unscaled_sixel_surf->w * sixel_scale), (int)((float)unscaled_sixel_surf->h * sixel_scale), 32,
                                      rmask, gmask, bmask, amask);
    if (sixel_surf != NULL) {
      dest.x = cur_col * 8;
      dest.y = cur_row * 16;
      dest.w = sixel_surf->w;
      dest.h = sixel_surf->h;

      SDL_BlitScaled(unscaled_sixel_surf, NULL, sixel_surf, NULL);

      SDL_FreeSurface(unscaled_sixel_surf);
      unscaled_sixel_surf = NULL;

      SDL_BlitSurface(sixel_surf, NULL, screenbacker, &dest);
      SDL_BlitSurface(sixel_surf, NULL, blink_screen, &dest);

      if (sixel_surf->h <= smodes[screen_mode].screen_height) {
        cur_row = sixel_surf->h / 16;

        SDL_FreeSurface(sixel_surf);
        sixel_surf = NULL;

        if (cur_row >= smodes[screen_mode].term_height) {
          cur_row = smodes[screen_mode].term_height - 1;
        }
      } else {
        sixel_surf_at = 0;
        cur_row = smodes[screen_mode].term_height - 1;
      }
    }
  }
}

void Terminal::clickLink(int x, int y) {
  std::regex urlr("(((gemini|gopher|news|(ht|f)tp(s?))\\://){1}\\S+)");
  std::stringstream ss;
  std::smatch match;
  std::string str;
  int i;
#ifndef _MSC_VER
  const char *path = getenv("PATH");
  char *path_copy = strdup(path);
  char *ptr2;
  struct stat s;
  int found = 0;
  char filename[PATH_MAX];
  pid_t pid;

#endif

  for (i = 0; i < smodes[screen_mode].term_width; i++) {
    if (scrollingBack) {
      ss << scrollback_buffer[i][scrollBackTop + y];
    } else {
      ss << screen_buffer[i][y];
    }
  }

  str = ss.str();
  std::regex_search(str, match, urlr);

  for (i = 0; i < match.size(); i++) {
    if (x >= match.position(i) && x <= match.position(i) + match.length(i)) {
      // got a match

#ifdef _MSC_VER
      ShellExecute(NULL, "open", match[i].str().c_str(), NULL, NULL, SW_SHOWNORMAL);
#else

      ptr2 = strtok(path_copy, ":");
      while (ptr2 != NULL) {
#ifdef __APPLE__
        snprintf(filename, PATH_MAX, "%s/open", ptr2);
#else
        snprintf(filename, PATH_MAX, "%s/xdg-open", ptr2);
#endif
        if (stat(filename, &s) == 0) {
          found = 1;
          break;
        }
        ptr2 = strtok(NULL, ":");
      }

      if (found) {
        pid = fork();
        if (pid == 0) {
          execlp(filename, filename, match[i].str().c_str(), NULL);
          exit(-1);
        } else if (pid < 0) {
          printf("error forking...\r\n");
        }
      }
      free(path_copy);
#endif
      return;
    }
  }
}

void Terminal::scrollback() {
  int j, x, y, i;
  SDL_Rect src, dest;
  scrollingBack = !scrollingBack;
  if (scrollingBack) {
    scrollBackTop = 75;
    SDL_FillRect(scrollback_screen, 0, 0xff000000);
    for (i = scrollBackTop; i < scrollBackTop + smodes[screen_mode].term_height; i++) {
      for (j = 0; j < smodes[screen_mode].term_width; j++) {
        src.x = (scrollback_buffer[j][i] % 32) * 8;
        src.y = (scrollback_buffer[j][i] / 32) * 16;
        src.w = 8;
        src.h = 16;

        dest.x = j * 8;
        dest.y = (i - scrollBackTop) * 16;
        dest.w = 8;
        dest.h = 16;
        SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
        SDL_LockSurface(scrollback_screen);
        for (x = dest.x; x < dest.x + dest.w; x++) {
          for (y = dest.y; y < dest.y + dest.h; y++) {
            if (getpixel(scrollback_screen, x, y) == 0xff000000) {
              putpixel(scrollback_screen, x, y, colours[7]);
            } else {
              putpixel(scrollback_screen, x, y, colours[0]);
            }
          }
        }
        SDL_UnlockSurface(scrollback_screen);
      }
    }
  }
}

void Terminal::scrollUp() {
  SDL_Rect src, dest;
  int i, j, x, y;
  src.x = 0;
  src.y = 16;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height - 16;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height - 16;
  SDL_FillRect(screencopy, 0, 0xff000000);
  SDL_BlitSurface(screenbacker, &src, screencopy, &dest);
  src.x = 0;
  src.y = 0;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height;
  SDL_FillRect(screenbacker, 0, 0xff000000);
  SDL_BlitSurface(screencopy, &src, screenbacker, &dest);
  src.x = 0;
  src.y = 16;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height - 16;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height - 16;
  SDL_FillRect(screencopy, 0, 0xff000000);
  SDL_BlitSurface(blink_screen, &src, screencopy, &dest);
  src.x = 0;
  src.y = 0;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height;
  SDL_FillRect(blink_screen, 0, 0xff000000);
  SDL_BlitSurface(screencopy, &src, blink_screen, &dest);
  for (i = 0; i < (smodes[screen_mode].term_height * 4) - 1; i++) {
    for (j = 0; j < smodes[screen_mode].term_width; j++) {
      scrollback_buffer[j][i] = scrollback_buffer[j][i + 1];
    }
  }

  for (j = 0; j < smodes[screen_mode].term_width; j++) {
    scrollback_buffer[j][(smodes[screen_mode].term_height * 4) - 1] = screen_buffer[j][0];
  }

  src.x = 0;
  src.y = 16;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height - 16;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height - 16;
  SDL_FillRect(screencopy, 0, 0xff000000);
  SDL_BlitSurface(scrollback_screen, &src, screencopy, &dest);
  src.x = 0;
  src.y = 0;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height;
  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height;
  SDL_FillRect(scrollback_screen, 0, 0xff000000);
  SDL_BlitSurface(screencopy, &src, scrollback_screen, &dest);

  for (j = 0; j < smodes[screen_mode].term_width; j++) {
    src.x = (scrollback_buffer[j][scrollBackTop + (smodes[screen_mode].term_height - 1)] % 32) * 8;
    src.y = (scrollback_buffer[j][scrollBackTop + (smodes[screen_mode].term_height - 1)] / 32) * 16;
    src.w = 8;
    src.h = 16;

    dest.x = j * 8;
    dest.y = (scrollBackTop + (smodes[screen_mode].term_height - 1)) * 16;
    dest.w = 8;
    dest.h = 16;
    SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
    SDL_LockSurface(scrollback_screen);
    for (x = dest.x; x < dest.x + dest.w; x++) {
      for (y = dest.y; y < dest.y + dest.h; y++) {
        if (getpixel(scrollback_screen, x, y) == 0xff000000) {
          putpixel(scrollback_screen, x, y, colours[7]);
        } else {
          putpixel(scrollback_screen, x, y, colours[0]);
        }
      }
    }
    SDL_UnlockSurface(scrollback_screen);
  }

  for (i = 0; i < smodes[screen_mode].term_height - 1; i++) {
    for (j = 0; j < smodes[screen_mode].term_width; j++) {
      screen_buffer[j][i] = screen_buffer[j][i + 1];
    }
  }
  for (j = 0; j < smodes[screen_mode].term_width; j++) {
    screen_buffer[j][smodes[screen_mode].term_height - 1] = ' ';
  }
}

char *Terminal::gettext(int sx, int sy, int ex, int ey) {
  int cols = ex - sx;
  int rows = ey - sy;
  int x, y;

  char *data = (char *)malloc(rows * (cols + 1) + 1);
  if (!data) {
    return NULL;
  }
  memset(data, 0, rows * (cols + 1) + 1);
  for (y = sy; y < ey; y++) {
    for (x = sx; x < ex; x++) {
      if (scrollingBack) {
        data[(y - sy) * (cols + 1) + (x - sx)] = scrollback_buffer[x][scrollBackTop + y];
      } else {
        data[(y - sy) * (cols + 1) + (x - sx)] = screen_buffer[x][y];
      }
    }
    if (rows > 1) {
      data[(y - sy) * (cols + 1) + cols] = '\n';
    }
  }
  return data;
}

void Terminal::drawChar(unsigned char c) {
  int x;
  int y;
  SDL_Rect src, dest;

  screen_buffer[cur_col][cur_row] = c;

  src.x = (c % 32) * 8;
  src.y = (c / 32) * 16;
  src.w = 8;
  src.h = 16;

  dest.x = cur_col * 8;
  dest.y = cur_row * 16;
  dest.w = 8;
  dest.h = 16;

  int ps1;

  if (!blinking && !bold) {
    ps1 = 0;
  } else if (bold && !blinking) {
    ps1 = 1;
  } else if (blinking && !bold) {
    ps1 = 2;
  } else if (blinking && bold) {
    ps1 = 3;
  }

  switch (curfont[ps1]) {
  case 0:
    SDL_BlitSurface(font_bmp, &src, screenbacker, &dest);
    break;
  case 37:
    SDL_BlitSurface(pot_noodle_bmp, &src, screenbacker, &dest);
    break;
  case 40:
    SDL_BlitSurface(topaz_plus_bmp, &src, screenbacker, &dest);
    break;
  case 39:
    SDL_BlitSurface(mk_plus_bmp, &src, screenbacker, &dest);
    break;
  case 41:
    SDL_BlitSurface(mk_bmp, &src, screenbacker, &dest);
    break;
  case 42:
    SDL_BlitSurface(topaz_bmp, &src, screenbacker, &dest);
    break;
  case 38:
    SDL_BlitSurface(mosoul_bmp, &src, screenbacker, &dest);
    break;
  default:
    // currently only support 8x16 font
    {
      struct term_font tf;
      bool found = false;
      for (size_t i = 0; i < custom_fonts.size(); i++) {
        if (custom_fonts.at(i).slot == curfont[ps1]) {
          tf = custom_fonts.at(i);
          found = true;
          break;
        }
      }

      if (found && tf.type == 1) {
        SDL_LockSurface(screenbacker);
        for (size_t fy = 0; fy < 16; fy++) {
          for (size_t fx = 0; fx < 8; fx++) {
            if (!(tf.data.at(c * 16 + fy) >> fx & 0x1)) {
              putpixel(screenbacker, dest.x + 8 - (fx + 1), dest.y + fy, 0xffffffff);
            } else {
              putpixel(screenbacker, dest.x + 8 - (fx + 1), dest.y + fy, 0xff000000);
            }
          }
        }
        SDL_UnlockSurface(screenbacker);
      } else {

        SDL_BlitSurface(font_bmp, &src, screenbacker, &dest);
      }
    }
    break;
  }

  SDL_LockSurface(screenbacker);
  SDL_LockSurface(blink_screen);

  for (x = dest.x; x < dest.x + dest.w; x++) {
    for (y = dest.y; y < dest.y + dest.h; y++) {
      if (!blinking) {
        if (getpixel(screenbacker, x, y) == 0xff000000) {
          putpixel(screenbacker, x, y, fg_colour);
          putpixel(blink_screen, x, y, fg_colour);
        } else {
          putpixel(screenbacker, x, y, bg_colour);
          putpixel(blink_screen, x, y, bg_colour);
        }
      } else {
        if (getpixel(screenbacker, x, y) == 0xff000000) {
          putpixel(screenbacker, x, y, fg_colour);
        } else {
          putpixel(screenbacker, x, y, bg_colour);
        }
        putpixel(blink_screen, x, y, bg_colour);
      }
    }
  }

  SDL_UnlockSurface(screenbacker);
  SDL_UnlockSurface(blink_screen);
  cur_col++;
}

void Terminal::sb_up() {
  int j, x, y, i;
  SDL_Rect src, dest;
  if (scrollBackTop == 0) {
    return;
  }
  scrollBackTop--;

  SDL_FillRect(scrollback_screen, 0, 0xff000000);
  for (i = scrollBackTop; i < scrollBackTop + smodes[screen_mode].term_height; i++) {
    for (j = 0; j < smodes[screen_mode].term_width; j++) {
      src.x = (scrollback_buffer[j][i] % 32) * 8;
      src.y = (scrollback_buffer[j][i] / 32) * 16;
      src.w = 8;
      src.h = 16;

      dest.x = j * 8;
      dest.y = (i - scrollBackTop) * 16;
      dest.w = 8;
      dest.h = 16;
      SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
      SDL_LockSurface(scrollback_screen);
      for (x = dest.x; x < dest.x + dest.w; x++) {
        for (y = dest.y; y < dest.y + dest.h; y++) {
          if (getpixel(scrollback_screen, x, y) == 0xff000000) {
            putpixel(scrollback_screen, x, y, colours[7]);
          } else {
            putpixel(scrollback_screen, x, y, colours[0]);
          }
        }
      }
      SDL_UnlockSurface(scrollback_screen);
    }
  }
}

void Terminal::sb_down() {
  int j, x, y, i;
  SDL_Rect src, dest;
  if (scrollBackTop == 75) {
    return;
  }
  scrollBackTop++;
  SDL_FillRect(scrollback_screen, 0, 0xff000000);
  for (i = scrollBackTop; i < scrollBackTop + smodes[screen_mode].term_height; i++) {
    for (j = 0; j < smodes[screen_mode].term_width; j++) {
      src.x = (scrollback_buffer[j][i] % 32) * 8;
      src.y = (scrollback_buffer[j][i] / 32) * 16;
      src.w = 8;
      src.h = 16;

      dest.x = j * 8;
      dest.y = (i - scrollBackTop) * 16;
      dest.w = 8;
      dest.h = 16;
      SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
      SDL_LockSurface(scrollback_screen);
      for (x = dest.x; x < dest.x + dest.w; x++) {
        for (y = dest.y; y < dest.y + dest.h; y++) {
          if (getpixel(scrollback_screen, x, y) == 0xff000000) {
            putpixel(scrollback_screen, x, y, colours[7]);
          } else {
            putpixel(scrollback_screen, x, y, colours[0]);
          }
        }
      }
      SDL_UnlockSurface(scrollback_screen);
    }
  }
}

void Terminal::twrite(std::string str) { twrite((unsigned char *)str.c_str(), str.size()); }

void Terminal::twrite(unsigned char *c, int len) {
  int i, j, k;
  SDL_Rect dest;
  char buf[256];

  for (i = 0; i < len; i++) {
    if (cur_col == smodes[screen_mode].term_width) {
      cur_col = 0;
      cur_row++;
    }
    if (cur_row >= smodes[screen_mode].term_height) {
      // scroll_up();
      scrollUp();
      cur_row = smodes[screen_mode].term_height - 1;
    }
    if (state == 0) {
      if (c[i] == 0x1b) {
        state = 1;
      } else {
        if (c[i] == '\r') {
          cur_col = 0;
        } else if (c[i] == '\n') {
          cur_row++;
          if (cur_row == smodes[screen_mode].term_height) {
            scrollUp();
            cur_row--;
          }
        } else if (c[i] == 7) {
          // beep
#if defined(_MSC_VER)
          MessageBeep(0);
#elif defined(__APPLE__)
          NSBeep();
#elif defined(SDL_VIDEO_DRIVER_X11)
          if (strcmp(SDL_GetCurrentVideoDriver(), "x11") == 0) {
            SDL_SysWMinfo info;
            SDL_VERSION(&info.version);
            SDL_GetWindowWMInfo(win, &info);
            XBell(info.info.x11.display, 100);
          }
#endif
        } else if (c[i] == 8) {
          cur_col--;
          if (cur_col < 0) {
            cur_col = 0;
          }
        } else if (c[i] == '\t') {
          for (int z = 0; z < tabstops.size(); z++) {
            if (tabstops.at(z) > cur_col) {
              cur_col = tabstops.at(z) - 1;
              break;
            }
          }
        } else if (c[i] == 12) {
          SDL_FillRect(screenbacker, 0, bg_colour);
          SDL_FillRect(blink_screen, 0, bg_colour);
          for (j = 0; j < 75; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              scrollback_buffer[k][j] = scrollback_buffer[k][j + smodes[screen_mode].term_height];
            }
          }
          for (j = 0; j < smodes[screen_mode].term_height; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              scrollback_buffer[k][j + (smodes[screen_mode].term_height * 4) - smodes[screen_mode].term_height] = screen_buffer[k][j];
              screen_buffer[k][j] = ' ';
            }
          }
          cur_col = 0;
          cur_row = 0;
        } else {
          if (c[i] == 24 && zmodem_detect == 0) {
            zmodem_detect = 1;
          } else if (c[i] == 'B' && zmodem_detect == 1) {
            zmodem_detect = 2;
          } else if (c[i] == '0' && zmodem_detect == 2) {
            zmodem_detect = 3;
          } else if (c[i] == '0' && zmodem_detect == 3) {
            if (zmodem != NULL) {

              *zmodem = 1;
              zm_inf.ptr = zmodem;
              zm_inf.download_path = download_path.c_str();
              dlthread = SDL_CreateThread(do_zmodem_download, "ZMODEM", &zm_inf);
            }
            zmodem_detect = 0;
            return;
          } else if (c[i] == '1' && zmodem_detect == 3) {
            if (zmodem != NULL) {
              *zmodem = 2;
              zm_inf.ptr = zmodem;
            }
            zmodem_detect = 0;
            return;
          } else {
            zmodem_detect = 0;
          }
          drawChar(c[i]);
        }
      }
    } else if (state == 1) {
      if (c[i] == '[') {
        state = 2;
        continue;
      } else if (c[i] == 'P') {
        // device control string;
        state = 100;
        dcs_string.str("");
        continue;
      } else if (c[i] == '(') {
        // state 9 swallows next char;
        state = 9;
      } else {
        state = 0;
        // printf("Not CSI %d\n", c[i]);
        continue;
      }
    } else if (state == 9) {
      // printf("Got ESC(%c\n", c[i]);
      state = 0;
    } else if (state == 2) {
      param_count = 0;
      for (j = 0; j < 16; j++) {
        params[j] = 0;
      }
      state = 3;
    }

    if (state == 3) {
      if (c[i] == ';') {
        if (param_count < 15) {
          param_count++;
        }
        continue;
      } else if (c[i] >= '0' && c[i] <= '9') {
        if (!param_count)
          param_count = 1;
        params[param_count - 1] = params[param_count - 1] * 10 + (c[i] - '0');
        continue;
      } else if (c[i] == '<') {
        gotcterm = true;
        continue;
      } else if (c[i] == '=') {
        gotcterm_equals = true;
      } else {
        state = 4;
      }
    }

    if (state == 4) {
      switch (c[i]) {
      case 'H':
      case 'f':
        if (params[0])
          params[0]--;
        if (params[1])
          params[1]--;

        cur_col = params[1];
        cur_row = params[0];
        if (cur_col > smodes[screen_mode].term_width - 1) {
          cur_col = smodes[screen_mode].term_width - 1;
        }

        if (cur_row > smodes[screen_mode].term_height - 1) {
          cur_row = smodes[screen_mode].term_height - 1;
        }

        state = 0;
        break;
      case 'A':
        if (param_count > 0) {
          cur_row -= params[0];
        } else {
          cur_row--;
        }

        if (cur_row < 0) {
          cur_row = 0;
        }
        state = 0;
        break;
      case 'B':
        if (param_count > 0) {
          cur_row += params[0];
        } else {
          cur_row++;
        }

        if (cur_row > smodes[screen_mode].term_height - 1) {
          cur_row = smodes[screen_mode].term_height - 1;
        }
        state = 0;
        break;
      case 'C':
        if (param_count > 0) {
          cur_col += params[0];
        } else {
          cur_col++;
        }

        if (cur_col > smodes[screen_mode].term_width - 1) {
          cur_col = smodes[screen_mode].term_width - 1;
        }
        state = 0;
        break;
      case 'D':
        if (param_count > 0) {
          cur_col -= params[0];
        } else {
          cur_col--;
        }

        if (cur_col < 0) {
          cur_col = 0;
        }

        state = 0;
        break;
      case 'E':
        if (param_count > 0) {
          cur_row += params[0];
        } else {
          cur_row++;
        }
        state = 0;
        break;
      case 's':
        save_col = cur_col;
        save_row = cur_row;

        state = 0;
        break;
      case 'u':
        cur_col = save_col;
        cur_row = save_row;

        state = 0;
        break;
      case 'm':
        for (int l = 0; l < param_count;) {
          if (params[l] == 38 || params[l] == 48) {
            if (params[l + 1] == 5 && (param_count == l + 3 || (param_count > l + 3 && params[l + 3] > 30))) {
              // 256 colour
              if (params[l] == 38) {
                if (params[l + 2] >= 0 && params[l + 2] <= 7) {
                  switch (params[l + 2]) {
                  case 0:
                    color = 0;
                    break;
                  case 1:
                    color = 4;
                    break;
                  case 2:
                    color = 6;
                    break;
                  case 3:
                    color = 2;
                    break;
                  case 4:
                    color = 1;
                    break;
                  case 5:
                    color = 5;
                    break;
                  case 6:
                    color = 3;
                    break;
                  case 7:
                    color = 7;
                    break;
                  }
                  fg_colour = colours[color];
                } else if (params[l + 2] >= 8 && params[l + 2] <= 15) {
                  switch (params[l + 2]) {
                  case 8:
                    color = 8;
                    break;
                  case 9:
                    color = 12;
                    break;
                  case 10:
                    color = 10;
                    break;
                  case 11:
                    color = 14;
                    break;
                  case 12:
                    color = 9;
                    break;
                  case 13:
                    color = 13;
                    break;
                  case 14:
                    color = 11;
                    break;
                  case 15:
                    color = 15;
                    break;
                  }
                  fg_colour = colours[color];
                } else if (params[l + 2] >= 16 && params[l + 2] <= 231) {
                  params[l + 2] -= 16;
                  uint8_t r = params[l + 2] / 36;
                  uint8_t g = params[l + 2] % 36 / 6;
                  uint8_t b = params[l + 2] % 36 % 6;

                  switch (r) {
                  case 0:
                    break;
                  case 1:
                    r = 95;
                    break;
                  case 2:
                    r = 135;
                    break;
                  case 3:
                    r = 175;
                    break;
                  case 4:
                    r = 215;
                    break;
                  case 5:
                    r = 255;
                    break;
                  }
                  switch (g) {
                  case 0:
                    break;
                  case 1:
                    g = 95;
                    break;
                  case 2:
                    g = 135;
                    break;
                  case 3:
                    g = 175;
                    break;
                  case 4:
                    g = 215;
                    break;
                  case 5:
                    g = 255;
                    break;
                  }
                  switch (b) {
                  case 0:
                    break;
                  case 1:
                    b = 95;
                    break;
                  case 2:
                    b = 135;
                    break;
                  case 3:
                    b = 175;
                    break;
                  case 4:
                    b = 215;
                    break;
                  case 5:
                    b = 255;
                    break;
                  }
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                  fg_colour = 0x000000ff | (b << 8) | (g << 16) | (r << 24);
#else
                  fg_colour = 0xff000000 | (b << 16) | (g << 8) | r;
#endif
                } else if (params[l + 2] >= 232 && params[l + 2] <= 255) {
                  uint8_t grey = (params[l + 2] - 232) * 10;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                  fg_colour = 0x000000ff | (grey << 8) | (grey << 16) | (grey << 24);
#else
                  fg_colour = 0xff000000 | (grey << 16) | (grey << 8) | grey;
#endif
                }

              } else if (params[l] == 48) {
                if (params[l + 2] >= 0 && params[l + 2] <= 7) {
                  switch (params[l + 2]) {
                  case 0:
                    bgcolor = 0;
                    break;
                  case 1:
                    bgcolor = 4;
                    break;
                  case 2:
                    bgcolor = 6;
                    break;
                  case 3:
                    bgcolor = 2;
                    break;
                  case 4:
                    bgcolor = 1;
                    break;
                  case 5:
                    bgcolor = 5;
                    break;
                  case 6:
                    bgcolor = 3;
                    break;
                  case 7:
                    bgcolor = 7;
                    break;
                  }
                  bg_colour = colours[bgcolor];
                } else if (params[l + 2] >= 8 && params[l + 2] <= 15) {
                  switch (params[l + 2]) {
                  case 8:
                    bgcolor = 8;
                    break;
                  case 9:
                    bgcolor = 12;
                    break;
                  case 10:
                    bgcolor = 10;
                    break;
                  case 11:
                    bgcolor = 14;
                    break;
                  case 12:
                    bgcolor = 9;
                    break;
                  case 13:
                    bgcolor = 13;
                    break;
                  case 14:
                    bgcolor = 11;
                    break;
                  case 15:
                    bgcolor = 15;
                    break;
                  }
                  bg_colour = colours[bgcolor];
                } else if (params[l + 2] >= 16 && params[l + 2] <= 231) {
                  params[l + 2] -= 16;
                  uint8_t r = params[l + 2] / 36;
                  uint8_t g = params[l + 2] % 36 / 6;
                  uint8_t b = params[l + 2] % 36 % 6;

                  switch (r) {
                  case 0:
                    break;
                  case 1:
                    r = 95;
                    break;
                  case 2:
                    r = 135;
                    break;
                  case 3:
                    r = 175;
                    break;
                  case 4:
                    r = 215;
                    break;
                  case 5:
                    r = 255;
                    break;
                  }
                  switch (g) {
                  case 0:
                    break;
                  case 1:
                    g = 95;
                    break;
                  case 2:
                    g = 135;
                    break;
                  case 3:
                    g = 175;
                    break;
                  case 4:
                    g = 215;
                    break;
                  case 5:
                    g = 255;
                    break;
                  }
                  switch (b) {
                  case 0:
                    break;
                  case 1:
                    b = 95;
                    break;
                  case 2:
                    b = 135;
                    break;
                  case 3:
                    b = 175;
                    break;
                  case 4:
                    b = 215;
                    break;
                  case 5:
                    b = 255;
                    break;
                  }
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                  bg_colour = 0x000000ff | (b << 8) | (g << 16) | (r << 24);
#else
                  bg_colour = 0xff000000 | (b << 16) | (g << 8) | r;
#endif
                } else if (params[l + 2] >= 232 && params[l + 2] <= 255) {
                  uint8_t grey = (params[l + 2] - 232) * 10;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                  bg_colour = 0x000000ff | (grey << 8) | (grey << 16) | (grey << 24);
#else
                  bg_colour = 0xff000000 | (grey << 16) | (grey << 8) | grey;
#endif
                }
              }

              l += 3;
            } else if (params[l + 1] == 2 && (param_count == l + 5 || (param_count > l + 5 && params[l + 5] > 30 && params[l + 5] < 49))) {
              // 24bit colour
              if (params[l] == 38) {
                uint8_t r = params[l + 2];
                uint8_t g = params[l + 3];
                uint8_t b = params[l + 4];
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                fg_colour = 0x000000ff | (b << 8) | (g << 16) | (r << 24);
#else
                fg_colour = 0xff000000 | (b << 16) | (g << 8) | r;
#endif
              } else if (params[l] == 48) {
                uint8_t r = params[l + 2];
                uint8_t g = params[l + 3];
                uint8_t b = params[l + 4];
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                bg_colour = 0x000000ff | (b << 8) | (g << 16) | (r << 24);
#else
                bg_colour = 0xff000000 | (b << 16) | (g << 8) | r;
#endif
              }

              l += 5;
            }
          } else {
            int fg_change = color;
            int bg_change = bgcolor;
            switch (params[l]) {
            case 0:
              color = 7;
              bgcolor = 0;
              bold = 0;
              blinking = 0;
              if (high_intensity_bg) {
                high_intensity_bg_enabled = 0;
              }
              break;
            case 1:
              bold = 1;
              blinking = 0;
              {
                switch (color) {
                case 0:
                  color = 8;
                  break;
                case 4:
                  color = 12;
                  break;
                case 2:
                  color = 10;
                  break;
                case 6:
                  color = 14;
                  break;
                case 1:
                  color = 9;
                  break;
                case 5:
                  color = 13;
                  break;
                case 3:
                  color = 11;
                  break;
                case 7:
                  color = 15;
                  break;
                default:
                  break;
                }
              }
              if (high_intensity_bg) {
                switch (bgcolor) {
                case 8:
                  bgcolor = 0;
                  break;
                case 12:
                  bgcolor = 4;
                  break;
                case 10:
                  bgcolor = 2;
                  break;
                case 14:
                  bgcolor = 6;
                  break;
                case 9:
                  bgcolor = 1;
                  break;
                case 13:
                  bgcolor = 5;
                  break;
                case 11:
                  bgcolor = 3;
                  break;
                case 15:
                  bgcolor = 7;
                  break;
                default:
                  break;
                }
                high_intensity_bg_enabled = 0;
              }
              break;
            case 2:
              bold = 0;
              blinking = 0;
              {
                switch (color) {
                case 8:
                  color = 0;
                  break;
                case 12:
                  color = 4;
                  break;
                case 10:
                  color = 2;
                  break;
                case 14:
                  color = 6;
                  break;
                case 9:
                  color = 1;
                  break;
                case 13:
                  color = 5;
                  break;
                case 11:
                  color = 3;
                  break;
                case 15:
                  color = 7;
                  break;
                default:
                  break;
                }
              }
              if (high_intensity_bg) {
                switch (bgcolor) {
                case 8:
                  bgcolor = 0;
                  break;
                case 12:
                  bgcolor = 4;
                  break;
                case 10:
                  bgcolor = 2;
                  break;
                case 14:
                  bgcolor = 6;
                  break;
                case 9:
                  bgcolor = 1;
                  break;
                case 13:
                  bgcolor = 5;
                  break;
                case 11:
                  bgcolor = 3;
                  break;
                case 15:
                  bgcolor = 7;
                  break;
                default:
                  break;
                }
                high_intensity_bg_enabled = 0;
              }
              break;
            case 6:
            case 5:
              if (high_intensity_bg) {
                switch (bgcolor) {
                case 0:
                  bgcolor = 8;
                  break;
                case 4:
                  bgcolor = 12;
                  break;
                case 2:
                  bgcolor = 10;
                  break;
                case 6:
                  bgcolor = 14;
                  break;
                case 1:
                  bgcolor = 9;
                  break;
                case 5:
                  bgcolor = 13;
                  break;
                case 3:
                  bgcolor = 11;
                  break;
                case 7:
                  bgcolor = 15;
                  break;
                default:
                  break;
                }
                high_intensity_bg_enabled = 1;
              }
              if (!blink_disabled) {
                blinking = 1;
              }
              {
                switch (color) {
                case 8:
                  color = 0;
                  break;
                case 12:
                  color = 4;
                  break;
                case 10:
                  color = 2;
                  break;
                case 14:
                  color = 6;
                  break;
                case 9:
                  color = 1;
                  break;
                case 13:
                  color = 5;
                  break;
                case 11:
                  color = 3;
                  break;
                case 15:
                  color = 7;
                  break;
                default:
                  break;
                }
              }
              break;
            case 30:
              if (bold) {
                color = 8;
              } else {
                color = 0;
              }
              break;
            case 31:
              if (bold) {
                color = 12;
              } else {
                color = 4;
              }
              break;
            case 32:
              if (bold) {
                color = 10;
              } else {
                color = 2;
              }
              break;
            case 33:
              if (bold) {
                color = 14;
              } else {
                color = 6;
              }
              break;
            case 34:
              if (bold) {
                color = 9;
              } else {
                color = 1;
              }
              break;
            case 35:
              if (bold) {
                color = 13;
              } else {
                color = 5;
              }
              break;
            case 36:
              if (bold) {
                color = 11;
              } else {
                color = 3;
              }
              break;
            case 37:
              if (bold) {
                color = 15;
              } else {
                color = 7;
              }
              break;
            case 39:
              color = 7;
              bold = false;
              break;
            case 40:
              if (high_intensity_bg_enabled) {
                bgcolor = 8;
              } else {
                bgcolor = 0;
              }
              break;
            case 41:
              if (high_intensity_bg_enabled) {
                bgcolor = 12;
              } else {
                bgcolor = 4;
              }
              break;
            case 42:
              if (high_intensity_bg_enabled) {
                bgcolor = 10;
              } else {
                bgcolor = 2;
              }
              break;
            case 43:
              if (high_intensity_bg_enabled) {
                bgcolor = 14;
              } else {
                bgcolor = 6;
              }
              break;
            case 44:
              if (high_intensity_bg_enabled) {
                bgcolor = 9;
              } else {
                bgcolor = 1;
              }
              break;
            case 45:
              if (high_intensity_bg_enabled) {
                bgcolor = 13;
              } else {
                bgcolor = 5;
              }
              break;
            case 46:
              if (high_intensity_bg_enabled) {
                bgcolor = 11;
              } else {
                bgcolor = 3;
              }
              break;
            case 47:
              if (high_intensity_bg_enabled) {
                bgcolor = 15;
              } else {
                bgcolor = 7;
              }
              break;
            case 49:
              bgcolor = 0;
              break;
            }
            if (bgcolor != bg_change) {
              bg_colour = colours[bgcolor];
            }
            if (color != fg_change) {
              fg_colour = colours[color];
            }
            l++;
          }
        }

        state = 0;
        break;
      case 't':
        if (param_count == 4) {
          if (params[0] == 0) {
            // bg colour
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
            bg_colour = 0x000000ff | (params[3] << 8) | (params[2] << 16) | (params[1] << 24);
#else
            bg_colour = 0xff000000 | (params[3] << 16) | (params[2] << 8) | params[1];
#endif
          } else if (params[0] == 1) {
            // fg colour
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
            fg_colour = 0x000000ff | (params[3] << 8) | (params[2] << 16) | (params[1] << 24);
#else
            fg_colour = 0xff000000 | (params[3] << 16) | (params[2] << 8) | params[1];
#endif
          }
        }
        state = 0;
        break;
      case 'G':
        if (param_count == 1) {
          cur_col = params[0] - 1;
          if (cur_col >= smodes[screen_mode].term_width) {
            cur_col = smodes[screen_mode].term_width - 1;
          } else if (cur_col < 0) {
            cur_col = 0;
          }
        }
        state = 0;
        break;
      case 'd':
        if (param_count == 1) {
          cur_row = params[0] - 1;
          if (cur_row >= smodes[screen_mode].term_height) {
            cur_row = smodes[screen_mode].term_height - 1;
          } else if (cur_row < 0) {
            cur_row = 0;
          }
        }
        state = 0;
        break;
      case 'J':
        if (params[0] == 0) {
          dest.x = 0;
          dest.y = 16 * (cur_row + 1);
          dest.w = smodes[screen_mode].term_width * 16;
          dest.h = (smodes[screen_mode].term_height - (cur_row + 1)) * 16;
          SDL_FillRect(screenbacker, &dest, bg_colour);
          SDL_FillRect(blink_screen, &dest, bg_colour);
          for (j = cur_row; j < smodes[screen_mode].term_height; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              screen_buffer[k][j] = ' ';
            }
          }

        } else if (params[0] == 1) {
          dest.x = 0;
          dest.y = 0;
          dest.w = smodes[screen_mode].term_width * 16;
          dest.h = (cur_row - 1) * 16;
          SDL_FillRect(screenbacker, &dest, bg_colour);
          SDL_FillRect(blink_screen, &dest, bg_colour);
          for (j = 0; j < cur_row; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              screen_buffer[k][j] = ' ';
            }
          }
        } else if (params[0] == 2) {
          SDL_FillRect(screenbacker, 0, bg_colour);
          SDL_FillRect(blink_screen, 0, bg_colour);
          for (j = 0; j < 75; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              scrollback_buffer[k][j] = scrollback_buffer[k][j + smodes[screen_mode].term_height];
            }
          }
          for (j = 0; j < smodes[screen_mode].term_height; j++) {
            for (k = 0; k < smodes[screen_mode].term_width; k++) {
              scrollback_buffer[k][j + 75] = screen_buffer[k][j];
              screen_buffer[k][j] = ' ';
            }
          }
          cur_col = 0;
          cur_row = 0;
        }
        state = 0;
        break;
      case 'K':
        if (params[0] == 0) {
          j = cur_col;
          for (k = j; k < smodes[screen_mode].term_width; k++) {
            drawChar(' ');
          }
          cur_col = j;
        } else if (params[0] == 1) {
          j = cur_col;
          cur_col = 0;
          for (k = 0; k < j; k++) {
            drawChar(' ');
          }
          cur_col = j;
        } else if (params[0] == 2) {
          j = cur_col;
          cur_col = 0;
          for (k = 0; k < smodes[screen_mode].term_width; k++) {
            drawChar(' ');
          }
          cur_col = j;
        }
        state = 0;
        break;
      case 'n':
        // report cursor position

        if (gotcterm_equals) {
          if (params[0] == 1) {
            int last_slot = 43;

            for (size_t k = 0; k < custom_fonts.size(); k++) {
              if (last_slot <= custom_fonts.at(k).slot) {
                last_slot = custom_fonts.at(k).slot + 1;
              }
            }
            snprintf(buf, 256, "\x1b[=1;%d;%d;%d;%d;%d;%dn", last_slot, prev_font_selection_result, curfont[0], curfont[1], curfont[2], curfont[3]);
            if (type == 0) {
              ssh_channel_write(chan, buf, strlen(buf));
            } else if (type == 1) {
              send(tsock, buf, strlen(buf), 0);
            }
          } else if (params[0] == 3) {
            snprintf(buf, 256, "\x1b[=3;16;8n");
            if (type == 0) {
              ssh_channel_write(chan, buf, strlen(buf));
            } else if (type == 1) {
              send(tsock, buf, strlen(buf), 0);
            }
          }
        } else {
          if (params[0] == 6) {
            if ((type == 0 && chan != NULL) || (type == 1 && tsock != 0)) {
              snprintf(buf, 10, "\x1b[%d;%dR", cur_row + 1, cur_col + 1);
              if (type == 0) {
                ssh_channel_write(chan, buf, strlen(buf));
              } else if (type == 1) {
                send(tsock, buf, strlen(buf), 0);
              }
            }
          }
        }
        state = 0;
        break;
      case '?':
        vt320_count = 0;
        for (j = 0; j < 16; j++) {
          vt320[j] = 0;
        }
        state = 5;
        break;
      case 27:
        state = 1;
        break;
      case ' ':
        state = 7;
        break;
      case '*':
        state = 8;
        break;

      case 'c':
        if (gotcterm) {
          if (params[0] == 0) {
            snprintf(buf, 256, "\x1b[<0;1;3;4;6c");
            if (type == 0) {
              ssh_channel_write(chan, buf, strlen(buf));
            } else if (type == 1) {
              send(tsock, buf, strlen(buf), 0);
            }
          }
        } else {
          if (params[0] == 0) {
            snprintf(buf, 256, "\x1b[=67;84;101;114;109;1;314c");
            if (type == 0) {
              ssh_channel_write(chan, buf, strlen(buf));
            } else if (type == 1) {
              send(tsock, buf, strlen(buf), 0);
            }
          }
        }
        state = 0;
        break;
      default:
        // fprintf(stderr, "Unkown ansi code %c\n", c[i]);
        state = 0;
        break;
      }
      gotcterm = false;
      gotcterm_equals = false;
    } else if (state == 5) {
      if (c[i] == ';') {
        if (vt320_count < 15) {
          vt320_count++;
        }
        continue;
      } else if (c[i] >= '0' && c[i] <= '9') {
        if (!vt320_count)
          vt320_count = 1;
        vt320[vt320_count - 1] = vt320[vt320_count - 1] * 10 + (c[i] - '0');
        continue;
      } else {
        state = 6;
      }
    } else if (state == 7) {
      if (c[i] == 'D') {
        int ps1 = 0;
        if (params[0] >= 0 && params[0] < 4) {
          ps1 = params[0];
        }

        // is font valid
        switch (params[1]) {
        case 0:
        case 37:
        case 38:
        case 39:
        case 40:
        case 41:
        case 42:
          prev_font_selection_result = 0;
          break;
        default:
          prev_font_selection_result = 1;
          for (size_t i = 0; i < custom_fonts.size(); i++) {
            if (custom_fonts.at(i).slot == params[1]) {
              prev_font_selection_result = 0;
              break;
            }
          }
          break;
        }

        curfont[ps1] = params[1];

        if (ps1 == 0) {
          curfont[1] = curfont[0];
          curfont[2] = curfont[0];
          curfont[3] = curfont[0];
        }
        // printf("Got font change request %d %d\n", params[0], params[1]);
      }
      state = 0;
    } else if (state == 8) {
      if (c[i] == 'r') {
        // printf("Got Speed Emulation Change Request\n");
      }
      state = 0;
    } else if (state == 100) {
      if (c[i] == '\x1b') {
        state = 101;
      } else {
        dcs_string << c[i];
      }
    } else if (state == 101) {
      if (c[i] == '\\') {
        // string terminator
        if (dcs_string.str().substr(0, 11) == "CTerm:Font:") {
          // loadable font
          int p1 = 0;
          std::string data;
          int type = 0;
          for (size_t j = 11; j < dcs_string.str().size(); j++) {
            if (dcs_string.str().at(j) >= '0' && dcs_string.str().at(j) <= '9') {
              p1 = p1 * 10 + (dcs_string.str().at(j) - '0');
            } else {
              std::string in = dcs_string.str().substr(j + 1);
              data = base64_decode(in);
              break;
            }
          }
          if ((data.size() / 256) == 16) {
            // 8x16 font
            type = 1;
          } else if (data.size() / 256 == 14) {
            // 8x14 font
            type = 2;
          } else {
            // 8x8 font
            type = 3;
          }
          struct term_font tf;

          tf.slot = p1;
          tf.type = type;
          tf.data = data;

          bool found = false;

          for (size_t i = 0; i < custom_fonts.size(); i++) {
            if (custom_fonts.at(i).slot == p1) {
              found = true;
              custom_fonts.at(i).type = type;
              custom_fonts.at(i).data = data;
              break;
            }
          }
          if (!found) {
            custom_fonts.push_back(tf);
          }
        } else {
          // sixel
          int sparams[2] = {0, 0};
          int sparamat = 0;
          for (size_t i = 0; i < dcs_string.str().size(); i++) {
            if (dcs_string.str().at(i) >= '0' && dcs_string.str().at(i) <= '9') {
              sparams[sparamat] = sparams[sparamat] * 10 + (dcs_string.str().at(i) - '0');
            } else if (dcs_string.str().at(i) == ';') {
              if (sparamat < 1) {
                sparamat++;
              }
            } else {
              if (dcs_string.str().at(i) == 'q') {
                // actually sixel
                display_sixel(dcs_string.str().substr(i + 1));
              } else {
                // printf("DCS String: %s\n", dcs_string.str().c_str());
              }
              break;
            }
          }
        }
      }
      state = 0;
    }
    if (state == 6) {

      if (c[i] == 'h') {
        for (j = 0; j < vt320_count; j++) {
          switch (vt320[j]) {
          case 25:
            cursor = 1;
            break;
          case 33:
            high_intensity_bg = 1;
            break;
          case 35:
            blink_disabled = 1;
            break;
          default:
            // printf("Got unknown v320 code %d\n", vt320[j]);
            break;
          }
        }
      } else if (c[i] == 'l') {
        for (j = 0; j < vt320_count; j++) {
          switch (vt320[j]) {
          case 25:
            cursor = 0;
            break;
          case 33:
            high_intensity_bg = 0;
            break;
          case 35:
            blink_disabled = 0;
            break;
          default:
            // printf("Got unknown v320 code %d\n", vt320[j]);
            break;
          }
        }
      }
      state = 0;
    }
  }
  return;
}

#ifdef _MSC_VER
std::string utf16ToUTF8(const std::wstring &s) {
  const int size = ::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, NULL, 0, 0, NULL);

  std::vector<char> buf(size);
  ::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, &buf[0], size, 0, NULL);

  return std::string(&buf[0]);
}
#endif

void Terminal::setZmodem(int *z) { zmodem = z; }

void Terminal::screenshot() {
  char sspath[PATH_MAX];
  char ssfile[PATH_MAX];
  struct passwd *pwd;
  struct tm *time_now;
  time_t timen;
  struct stat st;
  int i = 1;
  // const Uint32 format = SDL_PIXELFORMAT_ARGB8888;
  const int width = smodes[screen_mode].screen_width;
  const int height = smodes[screen_mode].screen_height;
  Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif
  SDL_Surface *sshot = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
  // SDL_Surface *sshot = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, format);

#ifdef _MSC_VER
  WCHAR *homedir;
  SHGetKnownFolderPath(FOLDERID_Pictures, 0, NULL, &homedir);
  snprintf(sspath, PATH_MAX, "%s", utf16ToUTF8(homedir).c_str());
#else
  pwd = getpwuid(getuid());
  snprintf(sspath, PATH_MAX, "%s", pwd->pw_dir);
#endif
  timen = time(NULL);

  if (stat(sspath, &st) != 0) {
    return;
  }

  time_now = localtime(&timen);
  snprintf(ssfile, PATH_MAX, "%s%cMagiTerm SS - %04d%02d%02d%02d%02d%02d.png", sspath, PATH_SEP, time_now->tm_year + 1900, time_now->tm_mon + 1,
           time_now->tm_mday, time_now->tm_hour, time_now->tm_min, time_now->tm_sec);
  while (stat(ssfile, &st) == 0) {
    snprintf(ssfile, PATH_MAX, "%s%cMagiTerm SS - %04d%02d%02d%02d%02d%02d-%d.png", sspath, PATH_SEP, time_now->tm_year + 1900, time_now->tm_mon + 1,
             time_now->tm_mday, time_now->tm_hour, time_now->tm_min, time_now->tm_sec, i);
    i++;
  }
  SDL_FillRect(sshot, 0, 0xFF000000);
  SDL_BlitSurface(screenbacker, 0, sshot, 0);
  IMG_SavePNG(sshot, ssfile);
}

void Terminal::toggleIceColours() {
  icecolours = !icecolours;

  if (icecolours) {
    high_intensity_bg = 1;
    blink_disabled = 1;
  } else {
    high_intensity_bg = 0;
    blink_disabled = 0;
  }
}

int Terminal::isIceEnabled() {
  if (high_intensity_bg && blink_disabled) {
    return 1;
  } else {
    return 0;
  }
}

SDL_Surface *Terminal::getSurface() {
  SDL_Rect src, dest;
  src.x = 0;
  src.y = 0;
  src.w = smodes[screen_mode].screen_width;
  src.h = smodes[screen_mode].screen_height;

  dest.x = 0;
  dest.y = 0;
  dest.w = smodes[screen_mode].screen_width;
  dest.h = smodes[screen_mode].screen_height;
  SDL_FillRect(screen, &src, 0xff000000);
  if (last_show <= SDL_GetTicks()) {
    blink_toggle = !blink_toggle;
    last_show = SDL_GetTicks() + 500;
  }
  if (scrollingBack) {
    SDL_BlitSurface(scrollback_screen, &src, screen, &dest);
  } else if (sixel_surf_at != -1 && sixel_surf != NULL) {
    sixel_surf_at += 2;
    src.x = 0;
    src.y = sixel_surf_at;
    src.w = sixel_surf->w;
    src.h = smodes[screen_mode].screen_height;

    SDL_BlitSurface(sixel_surf, &src, screenbacker, &dest);
    SDL_BlitSurface(sixel_surf, &src, blink_screen, &dest);

    SDL_BlitSurface(screenbacker, NULL, screen, NULL);

    if (sixel_surf_at >= sixel_surf->h - smodes[screen_mode].screen_height) {
      sixel_surf_at = -1;
      SDL_FreeSurface(sixel_surf);
      sixel_surf = NULL;
    }
  } else {
    if (!blink_toggle) {
      SDL_BlitSurface(screenbacker, &src, screen, &dest);
    } else {
      SDL_BlitSurface(blink_screen, &src, screen, &dest);
    }

    if (cursor) {
      src.x = cur_col * 8;
      src.y = cur_row * 16 + 14;
      src.w = 8;
      src.h = 2;

      SDL_FillRect(screen, &src, 0xffffffff);
    }
  }
  return screen;
}

Terminal::~Terminal() {
  // dtor
  for (int i = 0; i < smodes[screen_mode].term_width; i++) {
    free(scrollback_buffer[i]);
    free(screen_buffer[i]);
  }
  free(scrollback_buffer);
  free(screen_buffer);
  SDL_FreeSurface(screen);
  SDL_FreeSurface(screenbacker);
  SDL_FreeSurface(screencopy);
  SDL_FreeSurface(blink_screen);
  SDL_FreeSurface(scrollback_screen);

  SDL_FreeSurface(font_bmp);
  SDL_FreeSurface(pot_noodle_bmp);
  SDL_FreeSurface(topaz_plus_bmp);
  SDL_FreeSurface(topaz_bmp);
  SDL_FreeSurface(mk_bmp);
  SDL_FreeSurface(mk_plus_bmp);
  SDL_FreeSurface(mosoul_bmp);
}

struct zmodem_buffer_t {
  unsigned char buffer;
  int occupied;
};

int lputs(void *cbdata, int lvl, const char *str) {
  if (lvl < 2) {
    return 0;
  }
  zm_type = 2;
  if (zm_status != NULL) {
    return 0;
  }
  zm_status = strdup(str);
  zm_update = 1;
  return 0;
}

void progress(void *cbdata, int64_t pos) {
  zm_type = 1;
  zm_value = pos;
  zm_update = 1;
  return;
}

int send_byte(void *cbdata, uint8_t byte, unsigned int timeout) {
  if (type == 0) {
    ssh_channel_write(chan, &byte, 1);
  } else if (type == 1) {
    send_telnet(tsock, (const char *)&byte, 1, 0);
  }
  sz = 1;
  return 0;
}

int recv_byte(void *cbdata, unsigned int timeout) {
  int len = 0;
  time_t now = time(NULL);
  time_t then = now + timeout;
  struct zmodem_buffer_t *zmbuf = (struct zmodem_buffer_t *)cbdata;
  unsigned char c;

  if (zmbuf->occupied) {
    c = zmbuf->buffer;
    zmbuf->occupied = 0;
    rz = 1;
    return (int)c;
  }

  do {
    if (type == 0) {
      len = ssh_channel_read_nonblocking(chan, &c, 1, 0);
    } else if (type == 1) {
      len = recv_telnet(tsock, (char *)&c, 1, 0);
    }
    if (len == 1) {
      rz = 1;
      return (int)c;
    }
    now = time(NULL);
  } while (now < then);

  return -1;
}

BOOL is_connected(void *cbdata) { return 1; }

BOOL is_cancelled(void *cbdata) { return 0; }

BOOL data_waiting(void *cbdata, unsigned int timeout) {
  time_t now = time(NULL);
  time_t then = now + timeout;
  struct zmodem_buffer_t *zmbuf = (struct zmodem_buffer_t *)cbdata;
  int len;
  do {
    if (!zmbuf->occupied) {
      if (type == 0) {
        len = ssh_channel_read_nonblocking(chan, &(zmbuf->buffer), 1, 0);
      } else if (type == 1) {
        len = recv_telnet(tsock, (char *)&(zmbuf->buffer), 1, 0);
      }
      if (len == 1) {
        zmbuf->occupied = 1;
        return 1;
      }
    } else {
      return 1;
    }
    now = time(NULL);
  } while (now < then);
  return 0;
}

void flush(void *cbdata) {}

int do_zmodem_download(void *ptr) {
  struct zmodem_info *zi = (struct zmodem_info *)ptr;
  int *zmodem = zi->ptr;
  uint64_t bytes_received = 0;
  struct zmodem_buffer_t zmbuf;
  struct stat s;

  memset(&zmbuf, 0, sizeof(struct zmodem_buffer_t));

  zmodem_t zm;

  ft = 1;

  if (stat(zi->download_path, &s) != 0) {
#ifdef _MSC_VER
    mkdir(zi->download_path);
#else
    mkdir(zi->download_path, 0755);
#endif
  }

  zmodem_init(&zm, &zmbuf, lputs, progress, send_byte, recv_byte, is_connected, is_cancelled, data_waiting, flush);

  if (type == 1) {
    zm.escape_telnet_iac = 1;
  }

  zmodem_recv_files(&zm, zi->download_path, &bytes_received);

  *zmodem = 0;
  ft = 0;
  return 0;
}

int do_zmodem_upload(void *ptr) {
  struct zmodem_info *zi = (struct zmodem_info *)ptr;
  int *zmodem = zi->ptr;
  struct zmodem_buffer_t zmbuf;
  time_t start;
  uint64_t sent;
  zmodem_t zm;
  char *bname;
  memset(&zmbuf, 0, sizeof(struct zmodem_buffer_t));

  ft = 1;
  zmodem_init(&zm, &zmbuf, lputs, progress, send_byte, recv_byte, is_connected, is_cancelled, data_waiting, flush);
  if (type == 1) {
    zm.escape_telnet_iac = 1;
  }

  for (int i = strlen(zi->uploadfile) - 1; i >= 0; i--) {
    if (zi->uploadfile[i] == '/' || zi->uploadfile[i] == '\\') {
      break;
    }
    bname = &(zi->uploadfile[i]);
  }

  FILE *ulfile = fopen(zi->uploadfile, "rb");

  if (ulfile) {
    zmodem_send_file(&zm, bname, ulfile, 0, &start, &sent);
  }
  fclose(ulfile);
  zmodem_send_zfin(&zm);
  ft = 0;
  *zmodem = 0;

  return 0;
}
