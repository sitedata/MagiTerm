#include "yesno.h"
#include "Terminal.h"

yesno::yesno(Terminal *term, std::string p) {
  t = term;
  prompt = p;
  highlighted = false;
}

void yesno::draw() {
  t->twrite("\x1b[0;46;30m\x1b[15;15H+--------------[Really Delete]----------------+");
  t->twrite("\x1b[16;15H|                                             |");
  t->twrite("\x1b[17;15H|                                             |");
  t->twrite("\x1b[18;15H|                                             |");
  t->twrite("\x1b[19;15H|                                             |");
  t->twrite("\x1b[20;15H|                                             |");
  t->twrite("\x1b[21;15H+---------------------------------------------+");
  t->twrite("\x1b[17;" + std::to_string(15 + (44 - prompt.size()) / 2) + "H" + prompt);
  if (highlighted) {
    t->twrite("\x1b[19;22H\x1b[0;37m[ (Y) Yes ]\x1b[0;46;30m");
    t->twrite("\x1b[19;43H[ (N) No ]");
  } else {
    t->twrite("\x1b[19;22H[ (Y) Yes ]");
    t->twrite("\x1b[19;43H\x1b[0;37m[ (N) No ]\x1b[0;46;30m");
  }
}

void yesno::tab() {
  highlighted = !highlighted;
  draw();
}

bool yesno::process(char *data, bool *out) {
  for (int i = 0; i < strlen(data); i++) {

    if (tolower(data[i]) == 'y' || tolower(data[i]) == 'n') {
      *out = (tolower(data[i]) == 'y');
      return true;
    }
  }
  return false;
}
